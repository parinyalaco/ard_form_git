<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crop extends Model
{
    protected $connection = 'sqlsrv2';

    protected $table = 'crops';

    protected $fillable = [
        'id', 'name', 'details', 'sap_code', 'startdate', 'enddate', 'linkurl', 'createdBy', 'modifiedBy', 'created', 'modified', 'max_per_day'
    ];
}
