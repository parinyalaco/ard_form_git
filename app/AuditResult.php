<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditResult extends Model
{
    protected $fillable = [
        'plan_audit_detail_id'
        ,'detail_question_id'
        ,'result_id', 'result_rate'
        ,'result_txt'
        ,'result_grade'
        ,'path1'
        ,'path2'
        ,'note'
    ];

    public function planauditdetail()
    {
        return $this->belongsTo('App\PlanAuditDetail', 'plan_audit_detail_id', 'id');
    }

    public function detailquestion()
    {
        return $this->belongsTo('App\DetailQuestion', 'detail_question_id', 'id');
    }
}
