<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sowing extends Model
{
    protected $connection = 'sqlsrv2';

    protected $table = 'sowings';

    protected $fillable = [
        'id', 'crop_id', 'farmer_id', 'input_item_id', 'harvest_type_id', 'land_size', 
        'item_value', 'gps_land', 'gps_seed', 'status', 'grow_rate', 'grow_date', 
        'grow_note', 'start_date', 'details', 'relate', 'n_pos', 's_pos', 'e_pos', 
        'w_pos', 'createdBy', 'modifiedBy', 'created', 'modified', 'area_id', 'prev1', 'prev2', 
        'prev3', 'head_id', 'user_id', 'current_land', 'fail_land', 'fail_date', 'fail_condition', 
        'yield_rate', 'name', 'amg_land', 'ph_water', 'ph_soy', 'land_status', 'seed_code_id', 'seed_pack_id', 
        'harvest_status', 'seed_code_text', 'seed_code_date', 'farmer_status', 'raw_json', 'yield_rate7', 'lat', 
        'lng', 'qa_status', 'qc_status', 'user_farmer_id', 'a2seed', 'a3seed', 'f_land', 'post_harvest_complain', 
        'post_harvest_qc', 'post_harvest_qa', 'post_harvest_er', 'harvest_stage'
    ];

    public function crop()
    {
        return $this->belongsTo('App\Crop', 'crop_id', 'id');
    }

    public function inputitem()
    {
        return $this->belongsTo('App\InputItem', 'input_item_id', 'id');
    }

    public function userfarmer()
    {
        return $this->belongsTo('App\UserFarmer', 'user_farmer_id', 'id');
    }

    public function gpxfiles()
    {
        return $this->hasMany('App\GpxFile', 'sowing_id');
    }
}
