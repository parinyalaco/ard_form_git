<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Farmer extends Model
{
    protected $connection = 'sqlsrv2';

    protected $table = 'farmers';

    protected $fillable = [
        'id', 'code', 'init', 'fname', 'lname', 'citizenid', 'address1', 'address2', 'address3', 'sub_cities', 'city_id', 'province_id', 'createdBy', 'modifiedBy', 'created', 'modified'
    ];
}
