<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainQuestion extends Model
{
    protected $fillable = ['group_question_id','name','desc','seq',];

    public function groupquestion()
    {
        return $this->belongsTo('App\GroupQuestion', 'group_question_id', 'id');
    }

    public function detailquestion()
    {
        return $this->hasMany('App\DetailQuestion', 'main_question_id');
    }
}
