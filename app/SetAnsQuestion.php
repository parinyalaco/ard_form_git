<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SetAnsQuestion extends Model
{
    protected $fillable = [
        'name', 'desc',
    ];

    public function ansquestion()
    {
        return $this->hasMany('App\AnsQuestion', 'set_ans_question_id');
    }
}
