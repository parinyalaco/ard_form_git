<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnsQuestion extends Model
{
    protected $fillable = [
        'name', 'desc', 'set_ans_question_id'
    ];

    public function setansquestion()
    {
        return $this->belongsTo('App\SetAnsQuestion', 'set_ans_question_id', 'id');
    }
}
