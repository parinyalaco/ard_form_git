<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanAudit extends Model
{
    protected $fillable = ['group_question_id', 'set_insect_disease_id','plan_date','name','desc','status', 'user_id'];

    public function groupquestion()
    {
        return $this->belongsTo('App\GroupQuestion', 'group_question_id', 'id');
    }

    public function setinsectdisease()
    {
        return $this->belongsTo('App\SetInsectDisease', 'set_insect_disease_id', 'id');
    }


    public function planauditdetail()
    {
        return $this->hasMany('App\PlanAuditDetail', 'plan_audit_id');
    }
}
