<?php

namespace App\Http\Middleware;

use Closure;

class IsReview
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user && ( $user->group->name == 'Admin' || $user->group->name == 'Auditreviewer') ) {
            return $next($request);
        }

        return redirect('/');
    }
}
