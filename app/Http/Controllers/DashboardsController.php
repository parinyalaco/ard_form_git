<?php

namespace App\Http\Controllers;

use App\PlanAuditDetail;
use App\Crop;
use App\Sowing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Auth;


class DashboardsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){
        $croplist = Crop::where('linkurl', '!=', '')->orderBy('startdate')->pluck('name', 'id');
        $crop = Crop::orderBy('startdate','desc')->first();

        $datrw = PlanAuditDetail::where('crop_id', $crop->id)
        ->groupBy('broker_id')
        ->select(
        DB::raw('broker_id , count(id) as audits')
        )->get();

        $data = array();

        foreach ($datrw as $dataObj) {
            $data[$dataObj->broker_id] = $dataObj->audits;
        }

        $dataset = DB::connection('sqlsrv2')->table('sowings')
        ->join('user_farmer', 'user_farmer.id', '=', 'sowings.user_farmer_id')
        ->join('brokers', 'brokers.id', '=', 'user_farmer.broker_id')
        ->select(DB::raw('user_farmer.broker_id, brokers.code,count(sowings.id) as countall'))
        ->where('sowings.crop_id', $crop->id)
        ->where('sowings.harvest_status', '!=', 'เสียหายทั้งหมด')
        ->groupBy(DB::raw('user_farmer.broker_id, brokers.code'))
        ->get();

        return view('dashboards.index', compact('dataset', 'data', 'crop', 'croplist'));

    }

    public function crop($crop_id){
        $croplist = Crop::where('linkurl','!=','')->orderBy('startdate')->pluck('name', 'id');
        $crop = Crop::findOrFail($crop_id);

        $datrw = PlanAuditDetail::where('crop_id', $crop->id)
        ->groupBy('broker_id')
        ->select(
            DB::raw('broker_id , count(id) as audits')
        )->get();

        $data = array();

        foreach ($datrw as $dataObj) {
            $data[$dataObj->broker_id] = $dataObj->audits;
        }

        $dataset = DB::connection('sqlsrv2')->table('sowings')
        ->join('user_farmer', 'user_farmer.id', '=', 'sowings.user_farmer_id')
        ->join('brokers', 'brokers.id', '=', 'user_farmer.broker_id')
        ->select(DB::raw('user_farmer.broker_id, brokers.code,count(sowings.id) as countall'))
        ->where('sowings.crop_id', $crop->id)
        ->where('sowings.harvest_status', '!=', 'เสียหายทั้งหมด')
        ->groupBy(DB::raw('user_farmer.broker_id, brokers.code'))
        ->get();

        return view('dashboards.index', compact('dataset', 'data', 'crop', 'croplist'));
    }
}
