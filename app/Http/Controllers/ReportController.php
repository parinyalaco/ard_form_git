<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlanAudit;
use App\MainQuestion;
use App\PlanAuditDetail;
use App\SetAnsQuestion;
use App\Mail\AuditReport;
use Illuminate\Support\Facades\Mail;

class ReportController extends Controller
{
    public function reportPage($plan_audit_id)
    {

        $dataRaw = PlanAudit::findOrFail($plan_audit_id);

        $mainquestionsRaw = MainQuestion::where('group_question_id', $dataRaw->group_question_id)->orderBy('seq')->get();

        $data = array();
        $mainquestions = array();
        $dataInsect = array();

        foreach ($dataRaw->planauditdetail as $planAuditDetailObj) {
            foreach ($planAuditDetailObj->auditresult as $auditresultObj) {
                if ($auditresultObj->result_txt == 'ไม่สอดคล้อง') {
                    $data[$auditresultObj->detail_question_id][] = $auditresultObj;
                }
            }

            $resultinsectdiseaseObj = $planAuditDetailObj
                ->resultinsectdisease()
                //->where('result_desc', 'ไม่สอดคล้อง')
                ->orderBy('result_grade', 'desc')
                ->first();
            if (!empty($resultinsectdiseaseObj)) {
                $dataInsect[$resultinsectdiseaseObj->planauditdetail->id] = $resultinsectdiseaseObj;
            }
        }
        foreach ($mainquestionsRaw as $mainquestionsObj) {
            foreach ($mainquestionsObj->detailquestion as $detailquestionObj) {
                if (isset($data[$detailquestionObj->id])) {
                    $mainquestions[$detailquestionObj->id] = $detailquestionObj;
                }
            }
        }

        $PlanAudit = $dataRaw;

        return view('reports.planauditsummary', compact('data', 'mainquestions', 'dataInsect', 'PlanAudit'));

    }

    public function reportByFarmerByAudit($plan_audit_detail_id)
    {
        $planAuditDetailObj = PlanAuditDetail::findOrFail($plan_audit_detail_id);

        $answersetraw = SetAnsQuestion::all();
        $answerset = array();
        foreach ($answersetraw as $answersetObj) {
            foreach ($answersetObj->ansquestion()->get() as $ansquestionObj) {
                $answerset[$answersetObj->id][$ansquestionObj->id] =  $ansquestionObj->name;
            }
        }
        return view('reports.planauditbyfarmer', compact('plan_audit_detail_id', 'planAuditDetailObj', 'answerset'));
   
    }

    public function mainAudit($PlanAuditId){
        $PlanAudit = PlanAudit::findOrFail($PlanAuditId);

        return view('reports.mainaudit',compact('PlanAudit'));
    }

    public function sendReport($plan_audit_id){
        $auditStaff = config('myconfig.emailtestlist');

        Mail::to($auditStaff)->send(new AuditReport($plan_audit_id));

        $uri = url()->previous();

        return redirect($uri);
    }
}
