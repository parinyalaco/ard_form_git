<?php

namespace App\Http\Controllers;

use App\AuditResult;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PlanAudit;
use App\GroupQuestion;
use App\Broker;
use App\Crop;
use App\UserFarmer;
use App\Farmer;
use App\Sowing;
use App\InputItem;
use App\MainQuestion;
use App\PlanAuditDetail;
use App\ResultInsectDisease;
use App\SetAnsQuestion;
use App\SetInsectDisease;
use App\ResultDetailInsectDisease;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Maatwebsite\Excel\Facades\Excel;
use File;
use Carbon\Carbon;
use Session;
use Auth;

class PlanAuditsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->middleware('reviewer');
        $perPage = 20;
        $PlanAudits = PlanAudit::orderBy('plan_date', 'desc')->paginate($perPage);

        return view('backEnd.PlanAudits.index', compact('PlanAudits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->middleware('reviewer');
        $groupquestionlist = GroupQuestion::pluck('name', 'id');
        $setinsectdiseaselist = SetInsectDisease::pluck('name', 'id');
        return view('backEnd.PlanAudits.create', compact('groupquestionlist', 'setinsectdiseaselist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->middleware('reviewer');
        $postdata = $request->all();
        $user = Auth::user();
        $postdata['user_id'] = $user->id;

        PlanAudit::create($postdata);



        Session::flash('message', ' added!');
        Session::flash('status', 'success');

        return redirect('PlanAudits');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id, Request $request)
    {
        $this->middleware('reviewer');
        $perPage = 10;
        $farmername = $request->get('search');
        $cropId = $request->get('crop_id');
        $brokerId = $request->get('broker_id');
        $fromDate = $request->get('from_date');
        $toDate = $request->get('to_date');

        $Farmer = new Farmer();
        $UserFarmer = new UserFarmer();
        $Sowing = new Sowing();
        if (!empty($farmername)) {
            $farmerlist = $Farmer->where('fname', 'LIKE', '%' . $farmername . '%')->orWhere('lname', 'LIKE', '%' . $farmername . '%')->pluck('id', 'id');
            $UserFarmer = $UserFarmer->whereIn('farmer_id', $farmerlist);
        }

        if (!empty($cropId)) {
            $UserFarmer = $UserFarmer->where('crop_id', $cropId);
        } else {
            $cropData = Crop::orderBy('startdate', 'desc')->first();
            $UserFarmer = $UserFarmer->where('crop_id', $cropData->id);
        }

        if (!empty($brokerId)) {
            $UserFarmer = $UserFarmer->where('broker_id', $brokerId);
        }

        $Sowing = $Sowing->whereIn('user_farmer_id', $UserFarmer->pluck('id', 'id'));
        if (!empty($fromDate) && !empty($toDate)) {
            $Sowing = $Sowing->whereBetween('start_date', array($fromDate, $toDate));
        }
        $sowingdata = $Sowing->paginate($perPage);;

        $PlanAudit = PlanAudit::findOrFail($id);

        $brokerlist = Broker::pluck('code', 'id')->prepend('== เลือก ==', '');

        $croplist = Crop::orderBy('startdate', 'desc')->pluck('name', 'id')->prepend('== เลือก ==', '');

        return view('backEnd.PlanAudits.show', compact('PlanAudit', 'brokerlist', 'croplist', 'sowingdata'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $PlanAudit = PlanAudit::findOrFail($id);
        $groupquestionlist = GroupQuestion::pluck('name', 'id');
        $setinsectdiseaselist = SetInsectDisease::pluck('name', 'id');

        return view('backEnd.PlanAudits.edit', compact('PlanAudit', 'groupquestionlist', 'setinsectdiseaselist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $postdata = $request->all();

        $PlanAudit = PlanAudit::findOrFail($id);
        $PlanAudit->update($postdata);

        Session::flash('message', ' updated!');
        Session::flash('status', 'success');

        return redirect('PlanAudits');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $PlanAudit = PlanAudit::findOrFail($id);

        $PlanAudit->delete();

        Session::flash('message', ' deleted!');
        Session::flash('status', 'success');

        return redirect('PlanAudits');
    }

    public function addAuditLand($plan_audit_id, $sowing_id, Request $request)
    {
        $chk = PlanAuditDetail::where('plan_audit_id', $plan_audit_id)->where('sowing_id', $sowing_id)->count();
        if ($chk == 0) {

            $sowingData = Sowing::where('id', $sowing_id)->first();

            $user = Auth::user();

            $tmpData = array();
            $tmpData['plan_audit_id'] = $plan_audit_id;
            $tmpData['crop_id'] = $sowingData->userfarmer->crop_id;
            $tmpData['broker_id'] = $sowingData->userfarmer->broker_id;
            $tmpData['sowing_id'] = $sowing_id;
            $tmpData['audit_date'] = date('Y-m-d');
            $tmpData['user_id'] = $user->id;



            foreach ($sowingData->gpxfiles()->where('status', 1)->get() as $item2) {

                $file = "http://lacopm3:8080" . $sowingData->crop->linkurl . "/Sowings/downLoadGpx/" . $item2->gpx_realname;
                $file_headers = @get_headers($file);
                if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found' || $file_headers[12] == 'Content-Length: 0') {
                    $exists = false;
                } else {
                    //var_dump($file_headers);
                    $gpx = simplexml_load_file($file);

                    if ($gpx === false) {
                    } else {
                        foreach ($gpx->trk as $trk) {
                            foreach ($trk->trkseg as $seg) {
                                foreach ($seg->trkpt as $pt) {
                                    $tmpData['lat'] = $pt["lat"];
                                    $tmpData['lng'] = $pt["lon"];
                                    break;
                                }
                                break;
                            }
                        }
                    }
                    unset($gpx);
                }
            }



            PlanAuditDetail::create($tmpData);
        }

        $this->_updateseq($sowing_id);

        $uri = url()->previous();

        return redirect($uri);
    }

    private function _updateseq($sowing_id)
    {
        $planauditstmp = PlanAuditDetail::where('sowing_id', $sowing_id)->pluck('plan_audit_id', 'plan_audit_id');
        $planaudits = PlanAudit::whereIn('id', $planauditstmp)->orderBy('plan_date')->pluck('id', 'id');

        $loop = 1;
        foreach ($planaudits as $key => $value) {
            $tmp = PlanAuditDetail::where('sowing_id', $sowing_id)->where('plan_audit_id', $value)->first();
            $tmp->seq = $loop;
            $tmp->update();
            $loop++;
        }
    }

    public function _initForm($plan_audit_detail_id)
    {
        $planAuditDetailObj = PlanAuditDetail::findOrFail($plan_audit_detail_id);

        if ($planAuditDetailObj->auditresult()->count() == 0) {
            $mainQuestionList = $planAuditDetailObj->planaudit->groupquestion->mainquestion()->orderBy('seq')->get();
            foreach ($mainQuestionList as $mainQuestionObj) {
                // echo $mainQuestionObj->name."<br/>";
                foreach ($mainQuestionObj->detailquestion()->orderBy('seq')->get() as $detailquestionObj) {
                    // echo $detailquestionObj->name."<br/>";
                    $tmp = array();
                    $tmp['plan_audit_detail_id'] = $plan_audit_detail_id;
                    $tmp['detail_question_id'] = $detailquestionObj->id;

                    AuditResult::create($tmp);
                }
            }
        }
    }

    public function updateAuditForms($plan_audit_detail_id)
    {
        $this->_initForm($plan_audit_detail_id);
        $planAuditDetailObj = PlanAuditDetail::findOrFail($plan_audit_detail_id);

        $answersetraw = SetAnsQuestion::all();
        $answerset = array();
        foreach ($answersetraw as $answersetObj) {
            foreach ($answersetObj->ansquestion()->get() as $ansquestionObj) {
                $answerset[$answersetObj->id][$ansquestionObj->id] =  $ansquestionObj->name;
            }
        }

        return view('backEnd.PlanAudits.mainform', compact('plan_audit_detail_id', 'planAuditDetailObj', 'answerset'));
    }

    public function updateAuditFormsAction($plan_audit_detail_id, Request $request)
    {
        $postData = $request->all();

        $answersetraw = SetAnsQuestion::all();
        $answerset = array();
        foreach ($answersetraw as $answersetObj) {
            foreach ($answersetObj->ansquestion()->get() as $ansquestionObj) {
                $answerset[$ansquestionObj->id] =  $ansquestionObj;
            }
        }

        $planAuditDetailObj = PlanAuditDetail::findOrFail($plan_audit_detail_id);

        foreach ($planAuditDetailObj->auditresult()->get() as $auditresultObj) {
            if (
                isset($postData['answer_id_' . $auditresultObj->id])
                || isset($postData['note_' . $auditresultObj->id])
                || isset($postData['remove_' . $auditresultObj->id])
                || $request->hasFile('path1_' . $auditresultObj->id)
            ) {
                $tmpauditresult = AuditResult::findOrFail($auditresultObj->id);

                if (isset($postData['answer_id_' . $auditresultObj->id])) {
                    $tmpauditresult->result_id = $postData['answer_id_' . $auditresultObj->id];
                    $tmpauditresult->result_txt = trim($answerset[$postData['answer_id_' . $auditresultObj->id]]->name);
                    $tmpauditresult->result_grade = trim($answerset[$postData['answer_id_' . $auditresultObj->id]]->desc);
                }
                if (isset($postData['note_' . $auditresultObj->id])) {
                    $tmpauditresult->note = $postData['note_' . $auditresultObj->id];
                }
                if (isset($postData['remove_' . $auditresultObj->id]) && $postData['remove_' . $auditresultObj->id] == 'remove') {
                    $tmpauditresult->path1 = null;
                }

                if ($request->hasFile('path1_' . $auditresultObj->id)) {
                    $image = $request->file('path1_' . $auditresultObj->id);
                    $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('images/' . $plan_audit_detail_id);
                    $image->move($destinationPath, $name);

                    $tmpauditresult->path1 = 'images/' . $plan_audit_detail_id  . "/" . $name;
                }

                $tmpauditresult->update();
            }
        }

        if (
            $request->hasFile('path1')
            || $request->hasFile('path2')
            || $request->hasFile('path3')
            || $request->hasFile('path4')
            || isset($postData['removepath1'])
            || isset($postData['removepath2'])
            || isset($postData['removepath3'])
            || isset($postData['removepath4'])
            || isset($postData['note'])
        ) {
            if (isset($postData['removepath1']) && $postData['removepath1'] == 'remove') {
                $planAuditDetailObj->path1 = null;
            }
            if (isset($postData['removepath2']) && $postData['removepath2'] == 'remove') {
                $planAuditDetailObj->path2 = null;
            }
            if (isset($postData['removepath3']) && $postData['removepath3'] == 'remove') {
                $planAuditDetailObj->path3 = null;
            }
            if (isset($postData['removepath4']) && $postData['removepath4'] == 'remove') {
                $planAuditDetailObj->path4 = null;
            }

            if ($request->hasFile('path1')) {
                $image = $request->file('path1');
                $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/' . $plan_audit_detail_id);
                $image->move($destinationPath, $name);

                $planAuditDetailObj->path1 = 'images/' . $plan_audit_detail_id  . "/" . $name;
            }

            if ($request->hasFile('path2')) {
                $image = $request->file('path2');
                $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/' . $plan_audit_detail_id);
                $image->move($destinationPath, $name);

                $planAuditDetailObj->path2 = 'images/' . $plan_audit_detail_id  . "/" . $name;
            }

            if ($request->hasFile('path3')) {
                $image = $request->file('path3');
                $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/' . $plan_audit_detail_id);
                $image->move($destinationPath, $name);

                $planAuditDetailObj->path3 = 'images/' . $plan_audit_detail_id  . "/" . $name;
            }

            if ($request->hasFile('path4')) {
                $image = $request->file('path4');
                $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/' . $plan_audit_detail_id);
                $image->move($destinationPath, $name);

                $planAuditDetailObj->path4 = 'images/' . $plan_audit_detail_id  . "/" . $name;
            }

            if (isset($postData['note'])) {
                $planAuditDetailObj->note = $postData['note'];
            }





            $planAuditDetailObj->update();
        }

        $total_result = AuditResult::where('plan_audit_detail_id', $plan_audit_detail_id)
            ->where('result_grade', '!=', '')
            ->orderBy('result_grade', 'desc')
            ->first();
        $total_result_count = AuditResult::where('plan_audit_detail_id', $plan_audit_detail_id)
            ->where('result_grade', '!=', '')
            ->count();
        if (!empty($total_result)) {
            $planAuditDetailObj->result_txt = $total_result->result_txt;
            $planAuditDetailObj->result_grade = $total_result->result_grade;
            $planAuditDetailObj->result_num = $total_result_count;


            $planAuditDetailObj->update();
        }

        //update total result





        $uri = url()->previous();

        return redirect($uri);
    }

    public function removedetail($plan_audit_detail_id)
    {
        $files = File::files(public_path('images\\' . $plan_audit_detail_id));

        File::delete($files);
        File::deleteDirectory(public_path('images\\' . $plan_audit_detail_id));

        AuditResult::where('plan_audit_detail_id', $plan_audit_detail_id)->delete();

        $PlanAuditDetail = PlanAuditDetail::findOrFail($plan_audit_detail_id);

        $sowingid = $PlanAuditDetail->sowing_id;




        $PlanAuditDetail->delete();

        $this->_updateseq($sowingid);

        Session::flash('message', ' deleted!');
        Session::flash('status', 'success');

        $uri = url()->previous();
        return redirect($uri);
    }

    public function _initInsect($plan_audit_detail_id, $audit_result_id)
    {
        $planAuditDetailObj = PlanAuditDetail::findOrFail($plan_audit_detail_id);

        if ($planAuditDetailObj->resultinsectdisease()->count() == 0) {
            $insectdiseaseList = $planAuditDetailObj->planaudit->setinsectdisease->insectdisease()->orderBy('seq')->get();
            foreach ($insectdiseaseList as $insectdiseaseObj) {

                $tmp = array();
                $tmp['set_insect_disease_id'] = $insectdiseaseObj->set_insect_disease_id;
                $tmp['insect_disease_id'] = $insectdiseaseObj->id;
                $tmp['plan_audit_detail_id'] = $plan_audit_detail_id;
                $tmp['audit_result_id'] = $audit_result_id;
                $ResultInsectDiseaseId = ResultInsectDisease::create($tmp)->id;

                foreach ($insectdiseaseObj->insectdiseasecase()->orderBy('seq')->get() as $insectdiseasecaseObj) {

                    $subtmp = array();

                    $subtmp['result_insect_disease_id'] = $ResultInsectDiseaseId;
                    $subtmp['set_insect_disease_id'] = $insectdiseaseObj->set_insect_disease_id;
                    $subtmp['insect_disease_id'] = $insectdiseaseObj->id;
                    $subtmp['insect_disease_case_id'] = $insectdiseasecaseObj->id;
                    $subtmp['plan_audit_detail_id'] = $plan_audit_detail_id;

                    ResultDetailInsectDisease::create($subtmp);
                }
            }
        }
    }

    public function updateInsectForms($plan_audit_detail_id, $audit_result_id)
    {
        $this->_initInsect($plan_audit_detail_id, $audit_result_id);
        $planAuditDetailObj = PlanAuditDetail::findOrFail($plan_audit_detail_id);
        return view('backEnd.PlanAudits.insectform', compact('plan_audit_detail_id', 'planAuditDetailObj', 'audit_result_id'));
    }

    public function updateInsectFormsAction($plan_audit_detail_id, $audit_result_id, Request $request)
    {
        $postData = $request->all();

        $planAuditDetailObj = PlanAuditDetail::findOrFail($plan_audit_detail_id);
        foreach ($planAuditDetailObj->resultinsectdisease()->get() as $resultinsectdisease) {

            if (
                !empty($postData['allresult_txt_' . $resultinsectdisease->id])
                || !empty($postData['allresult_case_' . $resultinsectdisease->id])
                || !empty($postData['allresult_grade_' . $resultinsectdisease->id])
                || !empty($postData['note' . $resultinsectdisease->id])
                || $request->hasFile('path1_' . $resultinsectdisease->id)
                || $request->hasFile('path2_' . $resultinsectdisease->id)
                || isset($postData['remove_path1_' . $resultinsectdisease->id])
                || isset($postData['remove_path2_' . $resultinsectdisease->id])
            ) {

                $tmpresultinsectdisease = ResultInsectDisease::findOrFail($resultinsectdisease->id);

                if (
                    isset($postData['remove_path1_' . $resultinsectdisease->id])
                    && $postData['remove_path1_' . $resultinsectdisease->id] == 'remove'
                ) {
                    $tmpresultinsectdisease->path1 = null;
                }
                if (
                    isset($postData['remove_path2_' . $resultinsectdisease->id])
                    && $postData['remove_path2_' . $resultinsectdisease->id] == 'remove'
                ) {
                    $tmpresultinsectdisease->path2 = null;
                }

                if (!empty($postData['allresult_txt_' . $resultinsectdisease->id])) {
                    $tmpresultinsectdisease->result_text = $postData['allresult_txt_' . $resultinsectdisease->id];
                }
                if (!empty($postData['allresult_case_' . $resultinsectdisease->id])) {
                    $tmpresultinsectdisease->result_desc = $postData['allresult_case_' . $resultinsectdisease->id];
                }
                if (!empty($postData['allresult_grade_' . $resultinsectdisease->id])) {
                    $tmpresultinsectdisease->result_grade = $postData['allresult_grade_' . $resultinsectdisease->id];
                }
                if (!empty($postData['note' . $resultinsectdisease->id])) {
                    $tmpresultinsectdisease->note = $postData['note' . $resultinsectdisease->id];
                }


                if ($request->hasFile('path1_' . $resultinsectdisease->id)) {
                    $image = $request->file('path1_' . $resultinsectdisease->id);
                    $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('images/' . $plan_audit_detail_id);
                    $image->move($destinationPath, $name);

                    $tmpresultinsectdisease->path1 = 'images/' . $plan_audit_detail_id  . "/" . $name;
                }

                if ($request->hasFile('path2_' . $resultinsectdisease->id)) {
                    $image = $request->file('path2_' . $resultinsectdisease->id);
                    $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('images/' . $plan_audit_detail_id);
                    $image->move($destinationPath, $name);

                    $tmpresultinsectdisease->path2 = 'images/' . $plan_audit_detail_id  . "/" . $name;
                }

                $tmpresultinsectdisease->update();
            }
            foreach ($resultinsectdisease->resultdetailinsectdisease as $resultdetailinsectdiseaseObj) {
                if (
                    !empty($postData['point1_' . $resultdetailinsectdiseaseObj->id])
                    || !empty($postData['point2_' . $resultdetailinsectdiseaseObj->id])
                    || !empty($postData['point3_' . $resultdetailinsectdiseaseObj->id])
                    || !empty($postData['point4_' . $resultdetailinsectdiseaseObj->id])
                    || !empty($postData['point5_' . $resultdetailinsectdiseaseObj->id])
                    || !empty($postData['result_' . $resultdetailinsectdiseaseObj->id])
                    || !empty($postData['result_txt_' . $resultdetailinsectdiseaseObj->id])
                ) {
                    $tmpresultinsectdiseasecase = ResultDetailInsectDisease::findOrFail($resultdetailinsectdiseaseObj->id);
                    if (!empty($postData['point1_' . $resultdetailinsectdiseaseObj->id])) {
                        $tmpresultinsectdiseasecase->point1 = $postData['point1_' . $resultdetailinsectdiseaseObj->id];
                    }
                    if (!empty($postData['point2_' . $resultdetailinsectdiseaseObj->id])) {
                        $tmpresultinsectdiseasecase->point2 = $postData['point2_' . $resultdetailinsectdiseaseObj->id];
                    }
                    if (!empty($postData['point3_' . $resultdetailinsectdiseaseObj->id])) {
                        $tmpresultinsectdiseasecase->point3 = $postData['point3_' . $resultdetailinsectdiseaseObj->id];
                    }
                    if (!empty($postData['point4_' . $resultdetailinsectdiseaseObj->id])) {
                        $tmpresultinsectdiseasecase->point4 = $postData['point5_' . $resultdetailinsectdiseaseObj->id];
                    }
                    if (!empty($postData['point5_' . $resultdetailinsectdiseaseObj->id])) {
                        $tmpresultinsectdiseasecase->point5 = $postData['point5_' . $resultdetailinsectdiseaseObj->id];
                    }
                    if (!empty($postData['result_' . $resultdetailinsectdiseaseObj->id])) {
                        $tmpresultinsectdiseasecase->point_result = $postData['result_' . $resultdetailinsectdiseaseObj->id];
                    }
                    if (!empty($postData['result_txt_' . $resultdetailinsectdiseaseObj->id])) {
                        $tmpresultinsectdiseasecase->txt_result = $postData['result_txt_' . $resultdetailinsectdiseaseObj->id];
                    }
                    $tmpresultinsectdiseasecase->update();
                }
            }
        }

        $resultfrominsect = $planAuditDetailObj->resultinsectdisease()->where('result_text', '!=', '')->orderBy('result_grade', 'desc')->first();

        //var_dump($resultfrominsect);
        if (!empty($resultfrominsect)) {
            $auditresultobj = AuditResult::findOrFail($audit_result_id);
            $auditresultobj->result_txt = $resultfrominsect->result_desc;
            $auditresultobj->result_grade = $resultfrominsect->result_grade;
            $auditresultobj->update();
        }else{
            if($postData['noissues'] == 'yes'){
                $auditresultobj = AuditResult::findOrFail($audit_result_id);
                $auditresultobj->result_txt = 'สอดคล้อง';
                $auditresultobj->result_grade = 'A';
                $auditresultobj->update();
            }
        }

        $total_result = AuditResult::where('plan_audit_detail_id', $plan_audit_detail_id)
            ->where('result_grade', '!=', '')
            ->orderBy('result_grade', 'desc')
            ->first();
        $total_result_count = AuditResult::where('plan_audit_detail_id', $plan_audit_detail_id)
            ->where('result_grade', '!=', '')
            ->count();

        if (!empty($total_result)) {
            $planAuditDetailObj->result_txt = $total_result->result_txt;
            $planAuditDetailObj->result_grade = $total_result->result_grade;
            $planAuditDetailObj->result_num = $total_result_count;


            $planAuditDetailObj->update();
        }

        $uri = url()->previous();
        return redirect($uri);
    }

    public function reportPage($plan_audit_id)
    {

        $dataRaw = PlanAudit::findOrFail($plan_audit_id);

        $mainquestionsRaw = MainQuestion::where('group_question_id', $dataRaw->group_question_id)->orderBy('seq')->get();

        $data = array();
        $mainquestions = array();
        $dataInsect = array();

        foreach ($dataRaw->planauditdetail as $planAuditDetailObj) {
            foreach ($planAuditDetailObj->auditresult as $auditresultObj) {
                if ($auditresultObj->result_txt == 'ไม่สอดคล้อง') {
                    $data[$auditresultObj->detail_question_id][] = $auditresultObj;
                }
            }

            $resultinsectdiseaseObj = $planAuditDetailObj
                ->resultinsectdisease()
               // ->where('result_desc', 'ไม่สอดคล้อง')
                ->orderBy('result_grade', 'desc')
                ->first();
            if (!empty($resultinsectdiseaseObj)) {
                $dataInsect[$resultinsectdiseaseObj->planauditdetail->id] = $resultinsectdiseaseObj;
            }
        }
        foreach ($mainquestionsRaw as $mainquestionsObj) {
            foreach ($mainquestionsObj->detailquestion as $detailquestionObj) {
                if (isset($data[$detailquestionObj->id])) {
                    $mainquestions[$detailquestionObj->id] = $detailquestionObj;
                }
            }
        }

        // var_dump($dataInsect);

        $filename = "ard_report_" . $plan_audit_id . "_" . date('ymdHi');

        Excel::create($filename, function ($excel) use ($data, $mainquestions, $dataInsect, $dataRaw) {
            $excel->sheet('Sh1', function ($sheet) use ($data, $mainquestions, $dataRaw) {
                $sheet->loadView('exports.planaudit')
                    ->with('data', $data)
                    ->with('mainquestions', $mainquestions)
                    ->with('PlanAudit', $dataRaw);
            });

            $excel->sheet('Sh2', function ($sheet) use ($dataInsect, $dataRaw) {
                $sheet->loadView('exports.planauditinsect')
                ->with('dataInsect', $dataInsect)
                ->with('PlanAudit', $dataRaw);
            });
        })->export('xlsx');



        //return view('exports.planauditinsect', compact('data', 'mainquestions', 'dataInsect'));

    }

    public function updategps($plan_audit_detail_id, $audit_result_id,$no,$lat,$lng){
        $planAuditDetailObj = PlanAuditDetail::findOrFail($plan_audit_detail_id);

            switch ($no) {
                case '1':
                    $planAuditDetailObj->lat1 = $lat;
                    $planAuditDetailObj->lng1 = $lng;
                    break;
                case '2':
                    $planAuditDetailObj->lat2 = $lat;
                    $planAuditDetailObj->lng2 = $lng;
                    break;
                case '3':
                    $planAuditDetailObj->lat3 = $lat;
                    $planAuditDetailObj->lng3 = $lng;
                    break;
                case '4':
                    $planAuditDetailObj->lat4 = $lat;
                    $planAuditDetailObj->lng4 = $lng;
                    break;
                case '5':
                    $planAuditDetailObj->lat5 = $lat;
                    $planAuditDetailObj->lng5 = $lng;
                    break;
            }

        $planAuditDetailObj->update();

        $uri = url()->previous();
        return redirect($uri);
        
    }

    public function removegps($plan_audit_detail_id, $audit_result_id, $no)
    {
        $planAuditDetailObj = PlanAuditDetail::findOrFail($plan_audit_detail_id);

        switch ($no) {
            case '1':
                $planAuditDetailObj->lat1 = null;
                $planAuditDetailObj->lng1 = null;
                break;
            case '2':
                $planAuditDetailObj->lat2 = null;
                $planAuditDetailObj->lng2 = null;
                break;
            case '3':
                $planAuditDetailObj->lat3 = null;
                $planAuditDetailObj->lng3 = null;
                break;
            case '4':
                $planAuditDetailObj->lat4 = null;
                $planAuditDetailObj->lng4 = null;
                break;
            case '5':
                $planAuditDetailObj->lat5 = null;
                $planAuditDetailObj->lng5 = null;
                break;
        }

        $planAuditDetailObj->update();

        $uri = url()->previous();
        return redirect($uri);
    }

    public function updatestatus($plan_audit_id,$status){

        $PlanAudit = PlanAudit::findOrFail($plan_audit_id);
        $PlanAudit->status = $status;
        $PlanAudit->update();

        Session::flash('message', ' updated!');
        Session::flash('status', 'success');

        $uri = url()->previous();
        return redirect($uri);
    }

    public function updatedetailstatus($plan_audit_detail_id, $status){
        $planAuditDetailObj = PlanAuditDetail::findOrFail($plan_audit_detail_id);
        $planAuditDetailObj->status = $status;
        $planAuditDetailObj->update();

        Session::flash('message', ' updated!');
        Session::flash('status', 'success');

        $uri = url()->previous();
        return redirect($uri);
    }
}
