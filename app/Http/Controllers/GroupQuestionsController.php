<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\GroupQuestion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class GroupQuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $GroupQuestions = GroupQuestion::all();

        return view('backEnd.GroupQuestions.index', compact('GroupQuestions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.GroupQuestions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        GroupQuestion::create($request->all());

        Session::flash('message', ' added!');
        Session::flash('status', 'success');

        return redirect('GroupQuestions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $GroupQuestion = GroupQuestion::findOrFail($id);

        return view('backEnd.GroupQuestions.show', compact('GroupQuestion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $GroupQuestion = GroupQuestion::findOrFail($id);

        return view('backEnd.GroupQuestions.edit', compact('GroupQuestion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $GroupQuestion = GroupQuestion::findOrFail($id);
        $GroupQuestion->update($request->all());

        Session::flash('message', ' updated!');
        Session::flash('status', 'success');

        return redirect('GroupQuestions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $GroupQuestion = GroupQuestion::findOrFail($id);

        $GroupQuestion->delete();

        Session::flash('message', ' deleted!');
        Session::flash('status', 'success');

        return redirect('GroupQuestions');
    }

}
