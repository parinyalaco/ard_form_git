<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MainQuestion;
use App\GroupQuestion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class MainQuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $MainQuestions = MainQuestion::orderBy('group_question_id','asc')->orderBy('seq', 'asc')->get();

        return view('backEnd.MainQuestions.index', compact('MainQuestions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $qroupquestionlist = GroupQuestion::where('status','Active')->pluck('name','id');
        return view('backEnd.MainQuestions.create',compact('qroupquestionlist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        MainQuestion::create($request->all());

        Session::flash('message', ' added!');
        Session::flash('status', 'success');

        return redirect('MainQuestions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $MainQuestion = MainQuestion::findOrFail($id);

        return view('backEnd.MainQuestions.show', compact('MainQuestion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $qroupquestionlist = GroupQuestion::where('status', 'Active')->pluck('name', 'id');
        $MainQuestion = MainQuestion::findOrFail($id);

        return view('backEnd.MainQuestions.edit', compact('MainQuestion', 'qroupquestionlist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $MainQuestion = MainQuestion::findOrFail($id);
        $MainQuestion->update($request->all());

        Session::flash('message', ' updated!');
        Session::flash('status', 'success');

        return redirect('MainQuestions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $MainQuestion = MainQuestion::findOrFail($id);

        $MainQuestion->delete();

        Session::flash('message', ' deleted!');
        Session::flash('status', 'success');

        return redirect('MainQuestions');
    }

}
