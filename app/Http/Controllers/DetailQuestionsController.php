<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MainQuestion;
use App\TypeQuestion;
use App\SetAnsQuestion;
use App\DetailQuestion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class DetailQuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $DetailQuestions = DetailQuestion::all();

        return view('backEnd.DetailQuestions.index', compact('DetailQuestions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $mainquestionlist  = MainQuestion::orderBy('seq','asc')->pluck('name', 'id');
        $typequestionlist = TypeQuestion::pluck('name', 'id');
        $setansquestionlist = SetAnsQuestion::pluck('name', 'id');
        return view('backEnd.DetailQuestions.create',compact('mainquestionlist', 'typequestionlist', 'setansquestionlist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        DetailQuestion::create($request->all());

        Session::flash('message', ' added!');
        Session::flash('status', 'success');

        return redirect('DetailQuestions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $DetailQuestion = DetailQuestion::findOrFail($id);

        return view('backEnd.DetailQuestions.show', compact('DetailQuestion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $DetailQuestion = DetailQuestion::findOrFail($id);
        $mainquestionlist  = MainQuestion::orderBy('seq', 'asc')->pluck('name', 'id');
        $typequestionlist = TypeQuestion::pluck('name', 'id');
        $setansquestionlist = SetAnsQuestion::pluck('name', 'id');

        return view('backEnd.DetailQuestions.edit', compact('DetailQuestion', 'mainquestionlist', 'typequestionlist', 'setansquestionlist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $DetailQuestion = DetailQuestion::findOrFail($id);
        $DetailQuestion->update($request->all());

        Session::flash('message', ' updated!');
        Session::flash('status', 'success');

        return redirect('DetailQuestions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $DetailQuestion = DetailQuestion::findOrFail($id);

        $DetailQuestion->delete();

        Session::flash('message', ' deleted!');
        Session::flash('status', 'success');

        return redirect('DetailQuestions');
    }

}
