<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Group;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class GroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Groups = Group::all();

        return view('backEnd.Groups.index', compact('Groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.Groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        Group::create($request->all());

        Session::flash('message', ' added!');
        Session::flash('status', 'success');

        return redirect('Groups');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $Group = Group::findOrFail($id);

        return view('backEnd.Groups.show', compact('Group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $Group = Group::findOrFail($id);

        return view('backEnd.Groups.edit', compact('Group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $Group = Group::findOrFail($id);
        $Group->update($request->all());

        Session::flash('message', ' updated!');
        Session::flash('status', 'success');

        return redirect('Groups');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $Group = Group::findOrFail($id);

        $Group->delete();

        Session::flash('message', ' deleted!');
        Session::flash('status', 'success');

        return redirect('Groups');
    }

}
