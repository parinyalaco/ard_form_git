<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SetAnsQuestion;
use App\AnsQuestion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class AnsQuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $AnsQuestions = AnsQuestion::all();

        return view('backEnd.AnsQuestions.index', compact('AnsQuestions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $setansquestionlist  = SetAnsQuestion::pluck('name', 'id');
        return view('backEnd.AnsQuestions.create',compact('setansquestionlist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        AnsQuestion::create($request->all());

        Session::flash('message', ' added!');
        Session::flash('status', 'success');

        return redirect('AnsQuestions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $AnsQuestion = AnsQuestion::findOrFail($id);

        return view('backEnd.AnsQuestions.show', compact('AnsQuestion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $AnsQuestion = AnsQuestion::findOrFail($id);
        $setansquestionlist  = SetAnsQuestion::pluck('name', 'id');

        return view('backEnd.AnsQuestions.edit', compact('AnsQuestion', 'setansquestionlist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $AnsQuestion = AnsQuestion::findOrFail($id);
        $AnsQuestion->update($request->all());

        Session::flash('message', ' updated!');
        Session::flash('status', 'success');

        return redirect('AnsQuestions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $AnsQuestion = AnsQuestion::findOrFail($id);

        $AnsQuestion->delete();

        Session::flash('message', ' deleted!');
        Session::flash('status', 'success');

        return redirect('AnsQuestions');
    }

}
