<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SetInsectDisease;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class SetInsectDiseasesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $SetInsectDiseases = SetInsectDisease::all();

        return view('backEnd.SetInsectDiseases.index', compact('SetInsectDiseases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.SetInsectDiseases.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        SetInsectDisease::create($request->all());

        Session::flash('message', ' added!');
        Session::flash('status', 'success');

        return redirect('SetInsectDiseases');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $SetInsectDisease = SetInsectDisease::findOrFail($id);

        return view('backEnd.SetInsectDiseases.show', compact('SetInsectDisease'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $SetInsectDisease = SetInsectDisease::findOrFail($id);

        return view('backEnd.SetInsectDiseases.edit', compact('SetInsectDisease'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $SetInsectDisease = SetInsectDisease::findOrFail($id);
        $SetInsectDisease->update($request->all());

        Session::flash('message', ' updated!');
        Session::flash('status', 'success');

        return redirect('SetInsectDiseases');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $SetInsectDisease = SetInsectDisease::findOrFail($id);

        $SetInsectDisease->delete();

        Session::flash('message', ' deleted!');
        Session::flash('status', 'success');

        return redirect('SetInsectDiseases');
    }

}
