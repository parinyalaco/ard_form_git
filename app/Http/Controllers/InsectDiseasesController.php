<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\InsectDisease;
use App\InsectDiseaseCase;
use App\SetInsectDisease;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class InsectDiseasesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $InsectDiseases = InsectDisease::all();

        return view('backEnd.InsectDiseases.index', compact('InsectDiseases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $setInsectDiseaseList = SetInsectDisease::pluck('name','id');
        return view('backEnd.InsectDiseases.create',compact('setInsectDiseaseList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        InsectDisease::create($request->all());

        Session::flash('message', ' added!');
        Session::flash('status', 'success');

        return redirect('InsectDiseases');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $InsectDisease = InsectDisease::findOrFail($id);

        return view('backEnd.InsectDiseases.show', compact('InsectDisease'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $InsectDisease = InsectDisease::findOrFail($id);
        $setInsectDiseaseList = SetInsectDisease::pluck('name', 'id');
        return view('backEnd.InsectDiseases.edit', compact('InsectDisease', 'setInsectDiseaseList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $InsectDisease = InsectDisease::findOrFail($id);
        $InsectDisease->update($request->all());

        Session::flash('message', ' updated!');
        Session::flash('status', 'success');

        return redirect('InsectDiseases');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $InsectDisease = InsectDisease::findOrFail($id);

        $InsectDisease->delete();

        Session::flash('message', ' deleted!');
        Session::flash('status', 'success');

        return redirect('InsectDiseases');
    }

    public function createdetail($insect_disease_id)
    {
        $InsectDisease = InsectDisease::findOrFail($insect_disease_id);
        return view('backEnd.InsectDiseases.createdetail', compact('InsectDisease'));
    }

    public function createdetailAction($insect_disease_id, Request $request)
    {
        $tmp = $request->all();
        $tmp['insect_disease_id'] = $insect_disease_id;
        InsectDiseaseCase::create($tmp);

        Session::flash('message', ' added!');
        Session::flash('status', 'success');

        return redirect('InsectDiseases/'. $insect_disease_id);
    }

    public function editdetail($insect_disease_case_id)
    {
        $InsectDiseaseCase = InsectDiseaseCase::findOrFail($insect_disease_case_id);
        return view('backEnd.InsectDiseases.editdetail', compact('InsectDiseaseCase'));
    }

    public function editdetailAction($insect_disease_case_id, Request $request)
    {
        $InsectDiseaseCase = InsectDiseaseCase::findOrFail($insect_disease_case_id);
        $InsectDiseaseCase->update($request->all());

        Session::flash('message', ' updated!');
        Session::flash('status', 'success');
        return redirect('InsectDiseases/'. $InsectDiseaseCase->insect_disease_id);
    }

}
