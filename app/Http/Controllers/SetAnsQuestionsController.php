<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SetAnsQuestion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class SetAnsQuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $SetAnsQuestions = SetAnsQuestion::all();

        return view('backEnd.SetAnsQuestions.index', compact('SetAnsQuestions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.SetAnsQuestions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        SetAnsQuestion::create($request->all());

        Session::flash('message', ' added!');
        Session::flash('status', 'success');

        return redirect('SetAnsQuestions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $SetAnsQuestion = SetAnsQuestion::findOrFail($id);

        return view('backEnd.SetAnsQuestions.show', compact('SetAnsQuestion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $SetAnsQuestion = SetAnsQuestion::findOrFail($id);

        return view('backEnd.SetAnsQuestions.edit', compact('SetAnsQuestion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $SetAnsQuestion = SetAnsQuestion::findOrFail($id);
        $SetAnsQuestion->update($request->all());

        Session::flash('message', ' updated!');
        Session::flash('status', 'success');

        return redirect('SetAnsQuestions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $SetAnsQuestion = SetAnsQuestion::findOrFail($id);

        $SetAnsQuestion->delete();

        Session::flash('message', ' deleted!');
        Session::flash('status', 'success');

        return redirect('SetAnsQuestions');
    }

}
