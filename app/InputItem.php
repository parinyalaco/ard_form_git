<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InputItem extends Model
{
    protected $connection = 'sqlsrv2';

    protected $table = 'input_items';

    protected $fillable = [ 
        'id', 'name', 'code', 'tradename', 'common_name', 
        'size', 'unit_id', 'pur_of_use', 'RM_Group', 'created', 'modified'
    ];
}
