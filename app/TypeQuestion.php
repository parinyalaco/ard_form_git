<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeQuestion extends Model
{
    protected $fillable = [
        'name', 'desc', 'role',
    ];
}
