<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultInsectDisease extends Model
{
    protected $fillable = ['set_insect_disease_id',
            'insect_disease_id',
            'plan_audit_detail_id',
            'audit_result_id',
            'result_text',
            'result_desc',
            'result_grade',
            'path1',
            'path2',
            'note',
            'user_id',];

    public function setinsectdisease()
    {
        return $this->belongsTo('App\SetInsectDisease', 'set_insect_disease_id', 'id');
    }

    public function insectdisease()
    {
        return $this->belongsTo('App\InsectDisease', 'insect_disease_id', 'id');
    }

    public function planauditdetail()
    {
        return $this->belongsTo('App\PlanAuditDetail', 'plan_audit_detail_id', 'id');
    }

    public function resultdetailinsectdisease()
    {
        return $this->hasMany('App\ResultDetailInsectDisease', 'result_insect_disease_id');
    }

    public function auditresult()
    {
        return $this->belongsTo('App\AuditResult', 'audit_result_id', 'id');
    }
    
}
