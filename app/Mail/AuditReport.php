<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\PlanAudit;
use App\MainQuestion;
use Maatwebsite\Excel\Facades\Excel;

class AuditReport extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    public $plan_audit_id, $planaudit;


    public function __construct($plan_audit_id)
    {
        $this->plan_audit_id = $plan_audit_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $dataRaw = PlanAudit::findOrFail($this->plan_audit_id);
        $plan_audit_id = $this->plan_audit_id;
        $this->planaudit = $dataRaw;
        $planaudit = $this->planaudit;
        $PlanAudit = $this->planaudit;

        $title = "รายงานการ Audit ประจำวันที่ ". $dataRaw->plan_date. " ". $dataRaw->name;

        $mainquestionsRaw = MainQuestion::where('group_question_id', $dataRaw->group_question_id)->orderBy('seq')->get();

        $data = array();
        $mainquestions = array();
        $dataInsect = array();

        foreach ($dataRaw->planauditdetail as $planAuditDetailObj) {
            foreach ($planAuditDetailObj->auditresult as $auditresultObj) {
                if ($auditresultObj->result_txt == 'ไม่สอดคล้อง') {
                    $data[$auditresultObj->detail_question_id][] = $auditresultObj;
                }
            }

            $resultinsectdiseaseObj = $planAuditDetailObj
                ->resultinsectdisease()
                //->where('result_desc', 'ไม่สอดคล้อง')
                ->orderBy('result_grade', 'desc')
                ->first();
            if (!empty($resultinsectdiseaseObj)) {
                $dataInsect[$resultinsectdiseaseObj->planauditdetail->id] = $resultinsectdiseaseObj;
            }
        }
        foreach ($mainquestionsRaw as $mainquestionsObj) {
            foreach ($mainquestionsObj->detailquestion as $detailquestionObj) {
                if (isset($data[$detailquestionObj->id])) {
                    $mainquestions[$detailquestionObj->id] = $detailquestionObj;
                }
            }
        }

    // var_dump($dataInsect);
        $filename = "ard_report_" . $this->plan_audit_id . "_" . date('ymdHi');

        Excel::create($filename, function ($excel) use ($data, $mainquestions, $dataInsect, $dataRaw) {
            $excel->sheet('Sh1', function ($sheet) use ($data, $mainquestions, $dataRaw) {
                $sheet->loadView('exports.planaudit')
                ->with('data', $data)
                ->with('mainquestions', $mainquestions)
                ->with('PlanAudit', $dataRaw);
            });

            $excel->sheet('Sh2', function ($sheet) use ($dataInsect, $dataRaw) {
                $sheet->loadView('exports.planauditinsect')
                ->with('dataInsect', $dataInsect)
                ->with('PlanAudit', $dataRaw);
            });
        })->store('xlsx', storage_path('excel/exports'));

        $filewithpath = storage_path('excel/exports') ."/". $filename.".xlsx";

            return $this->view('emails.auditreport', compact('plan_audit_id', 'planaudit', 'PlanAudit', 'mainquestions', 'dataInsect','data'))
            ->subject($title)
            ->attach($filewithpath);
        
    }
}
