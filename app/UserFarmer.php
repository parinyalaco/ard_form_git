<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFarmer extends Model
{
    protected $connection = 'sqlsrv2';

    protected $table = 'user_farmer';

    protected $fillable = [
        'id', 'crop_id', 'user_id', 'manager_id', 'broker_id', 'area_id', 'farmer_id', 
        'head_id', 'sowing_city', 'farmer_code', 'status', 
        'createdBy', 'modifiedBy', 'created', 'modified'
    ];

    public function ae()
    {
        return $this->belongsTo('App\Ae', 'user_id', 'id');
    }

    public function supae()
    {
        return $this->belongsTo('App\Ae', 'manager_id', 'id');
    }

    public function broker()
    {
        return $this->belongsTo('App\Broker', 'broker_id', 'id');
    }

    public function head()
    {
        return $this->belongsTo('App\Head', 'head_id', 'id');
    }

    public function farmer()
    {
        return $this->belongsTo('App\Farmer', 'farmer_id', 'id');
    }
}
