<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectDisease extends Model
{
    protected $fillable = [
            'set_insect_disease_id',
            'type',
            'name',
            'desc',
            'seq',
            'conds',
            'results',
            'result_txts',
            'grades',
            'status',];

    public function setinsectdisease()
    {
        return $this->belongsTo('App\SetInsectDisease', 'set_insect_disease_id', 'id');
    }

    public function insectdiseasecase()
    {
        return $this->hasMany('App\InsectDiseaseCase', 'insect_disease_id');
    }
}
