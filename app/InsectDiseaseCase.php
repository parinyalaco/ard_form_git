<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectDiseaseCase extends Model
{
    protected $fillable = ['set_insect_disease_id',
            'insect_disease_id',
            'name',
            'desc',
            'seq',
            'method',
            'conds',
            'result_text',
            'result_desc',
            'result_grade', 'status',];
}
