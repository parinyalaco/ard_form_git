<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupQuestion extends Model
{
    protected $fillable = [
        'name', 'desc','status','seq'
    ];

    public function mainquestion()
    {
        return $this->hasMany('App\MainQuestion', 'group_question_id');
    }
}
