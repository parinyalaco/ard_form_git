<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultDetailInsectDisease extends Model
{
    protected $fillable = [
        'result_insect_disease_id',
        'set_insect_disease_id',
            'insect_disease_id',
            'insect_disease_case_id',
            'plan_audit_detail_id',
            'point1',
            'point2',
            'point3',
            'point4',
            'point5',
            'path1',
            'path2',
            'path3',
            'path4',
            'path5',
            'point_result',
            'txt_result',
            'user_id',];

    public function resultinsectdisease()
    {
        return $this->belongsTo('App\ResultInsectDisease', 'result_insect_disease_id', 'id');
    }

    public function insectdiseasecase()
    {
        return $this->belongsTo('App\InsectDiseaseCase', 'insect_disease_case_id', 'id');
    }
}
