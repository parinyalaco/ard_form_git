<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SetInsectDisease extends Model
{
    protected $fillable = [ 'name', 'desc', 'status',];

    public function insectdisease()
    {
        return $this->hasMany('App\InsectDisease', 'set_insect_disease_id');
    }
}
