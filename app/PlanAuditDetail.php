<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanAuditDetail extends Model
{

    protected $fillable = [
        'plan_audit_id', 'crop_id', 'broker_id', 'sowing_id', 'lat', 'lng', 'audit_date', 
        'path1', 'path2', 'path3', 'path4', 'note', 'user_id',
        'result_txt','result_grade','result_num',
        'seq',
            'lat1',
            'lng1',
        'lat2',
        'lng2',
        'lat3',
        'lng3',
        'lat4',
        'lng4',
        'lat5',
        'lng5'];

    public function planaudit()
    {
        return $this->belongsTo('App\PlanAudit', 'plan_audit_id', 'id');
    }

    public function sowing()
    {
        return $this->belongsTo('App\Sowing', 'sowing_id', 'id');
    }

    public function auditresult()
    {
        return $this->hasMany('App\AuditResult', 'plan_audit_detail_id');
    }

    public function resultinsectdisease()
    {
        return $this->hasMany('App\ResultInsectDisease', 'plan_audit_detail_id');
    }
}
