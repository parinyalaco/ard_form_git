<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GpxFile extends Model
{
    protected $connection = 'sqlsrv2';

    protected $table = 'gpx_files';

    protected $fillable = [
        'id', 'crop_id', 'sowing_id', 'gpxname', 'gpx_path', 'gpx_realname', 
        'status', 'created', 'modified'
    ];
}
