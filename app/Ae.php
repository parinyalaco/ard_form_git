<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ae extends Model
{
    protected $connection = 'sqlsrv2';

    protected $table = 'users';

    protected $fillable = [
        'id', 'username', 'password', 'email', 'init', 'fname', 'lname', 
        'citizenid', 'group_id', 'canEdit', 'status', 'created', 'modified'
    ];
}
