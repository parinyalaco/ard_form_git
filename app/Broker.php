<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Broker extends Model
{
    protected $connection = 'sqlsrv2';

    protected $table = 'brokers';

    protected $fillable = [
        'id', 'code', 'init', 'fname', 'lname', 'citizenid', 'address1', 
        'address2', 'address3', 'sub_cities', 'city_id', 'province_id', 
        'loc', 'broker_color', 'createdBy', 'modifiedBy', 'created', 'modified'
    ];
}
