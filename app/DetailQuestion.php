<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailQuestion extends Model
{
    protected $fillable = ['main_question_id','type_question_id','set_ans_question_id','name','desc','seq','link'];

    public function mainquestion()
    {
        return $this->belongsTo('App\MainQuestion', 'main_question_id', 'id');
    }

    public function typequestion()
    {
        return $this->belongsTo('App\TypeQuestion', 'type_question_id', 'id');
    }

    public function setansquestion()
    {
        return $this->belongsTo('App\SetAnsQuestion', 'set_ans_question_id', 'id');
    }
}
