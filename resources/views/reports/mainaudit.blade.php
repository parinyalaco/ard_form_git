@extends('layouts.app')

@section('content')
<div class="container">



    <div class="row">
<h1>รายงาน Audit {{ $PlanAudit->name }} ของวันที่ {{ $PlanAudit->plan_date }} ({{ $PlanAudit->status }})</h1>
<h3>{{ $PlanAudit->desc }} 
<a href="{{ url('PlanAudits/reportPage/'.$PlanAudit->id) }}" class="pull-right" style="padding-left: 10px;"><img src="{{ url('/img/excel.png') }}"></a>&nbsp;
    <a href="{{ url('Reports/reportPage/'.$PlanAudit->id) }}" class="pull-right" style="padding-left: 10px;"><img src="{{ url('/img/news.png') }}"></a>
</h3>

<h1>รายการลูกสวนที่เลือกมาตรวจสอบ</h1>
<div class="table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr> <th>Crop</th> 
                    <th>Broker</th> 
                    <th>Farmer</th> 
                    <th>พันธุ์</th> 
                    <th>วันปลูก</th>
                    <th>อายุ</th>
                    <th>สภาพ</th>
                    <th>ครั้งที่</th>
                    <th>พื้นที่</th> 
                    <th>ผล</th> 
                    <th>รายงาน</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $now = new DateTime($PlanAudit->plan_date);
                @endphp
                @foreach ($PlanAudit->planauditdetail as $item)
                  <tr>
                      <td>{{ $item->sowing->crop->name }}</td> 
                    <td>{{ $item->sowing->userfarmer->broker->code }}</td> 
                    <td>{{ $item->sowing->userfarmer->farmer->fname }} {{ $item->sowing->userfarmer->farmer->lname }}</td> 
                    
                    <td>{{ $item->sowing->InputItem->tradename}}</td> 
                    <td>{{ $item->sowing->start_date}}</td>  
                    <td>
                        @php
                            $OldDate = new DateTime($item->sowing->start_date);
    
    $result = $OldDate->diff($now);
    echo $result->days;
                        @endphp
                    </td>
                    <td>{{ $item->sowing->harvest_status}}</td>
                <td>{{ $item->seq }}</td>
                    <td>{{ $item->sowing->current_land }}
                        @if (!empty($item->lat) && !empty($item->lng))
                        <a href="http://www.google.com/maps/place/{{$item->lat}},{{$item->lng}}" target="_blank">
                            <img src="{{url('/img/map.png')}}" alt="Image"/></a>
                            
                        @endif
                    </td> 
                    <td>
                        @if (!empty($item->result_txt))
                          {{ $item->result_txt }} <br/>
                          เกรด {{ $item->result_grade }} / 
                          จากผล {{ $item->result_num }}/{{ $item->auditresult()->count() }} ข้อ
                        @endif
                        
                    </td>
                    
                <td><a href="{{ url('/Reports/reportByFarmerByAudit/'.$item->id) }}" target="_blank" ><img src="{{url('/img/report.png')}}" alt="Image"/> </a> 
                    </td>
                </tr>  
                @endforeach
                
            </tbody>    
        </table>
       
</div>
</div></div>
@endsection