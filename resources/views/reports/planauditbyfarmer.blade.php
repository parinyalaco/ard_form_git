@extends('layouts.app')

@section('content')
<div class="container">



    <div class="row">
<h1>{{ $planAuditDetailObj->sowing->userfarmer->broker->code }} / {{ $planAuditDetailObj->sowing->userfarmer->farmer->fname }} {{ $planAuditDetailObj->sowing->userfarmer->farmer->lname }}</h1>
<h2>{{ $planAuditDetailObj->sowing->inputitem->tradename }} ปลูกวันที่ {{ $planAuditDetailObj->sowing->start_date }} พื้นที่ {{ $planAuditDetailObj->sowing->current_land }} ไร่ อายุ
    <a href="{{ url('PlanAudits/'.$planAuditDetailObj->plan_audit_id) }}" class="btn btn-default pull-right btn-sm">Back</a>
    
   @php
         $OldDate = new DateTime($planAuditDetailObj->sowing->start_date);
    $now = new DateTime(Date('Y-m-d'));
    $result = $OldDate->diff($now);
    echo $result->days;
    @endphp
    วัน</h2>
    <hr/>
GE=ระดับความเสียหายน้อย, ET=ระดับความเสียหายปานกลาง, EL=ระดับความเสียหายวิกฤต
    <div class="table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="4" style="text-align: center;">จุดควบคุม และเกณฑ์ความสอดคล้องในการตรวจประเมินเกษตรกร</th>
                </tr>
                <tr>
                    <th width="50%">จุดควบคุม (Control Point)  เกษตรกร</th> 
                    <th>ระดับ (Level)</th> 
                    <th width="15%">ผลการตรวจสอบ</th>
                    <th>รายละเอียด</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $maintext = "";
                @endphp
                @foreach ($planAuditDetailObj->auditresult()->get() as $item)
                    @if ($maintext <> $item->detailquestion->mainquestion->name )
                        @php
                            $maintext = $item->detailquestion->mainquestion->name
                        @endphp
                        <tr>
                        <td colspan="4"><b>{{ $maintext }}</b></td>
                        </tr>                        
                    @endif
                    <tr>
                    <td>
                        {{ $item->detailquestion->name }}                   
                    </td>
                    <td>{{ $item->detailquestion->typequestion->name }}</td>    
                    <td>
                       
                            {{ $item->result_txt }}
                       

                    </td>
                    <td>
                    {{ $item->note or ''}}
                            @if (!empty($item->path1))
                                <br/><a href="{{ url($item->path1) }}" target="_blank"><img height="100px" src="{{ url($item->path1) }}" ></a>
                            @endif
  
                    </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4">{{ $planAuditDetailObj->note or ''}}
                        @if (!empty($planAuditDetailObj->path1))
                            <br/><a href="{{ url($planAuditDetailObj->path1) }}" target="_blank"><img height="100px" src="{{ url($planAuditDetailObj->path1) }}" ></a>
                        @endif
                        @if (!empty($planAuditDetailObj->path2))
                            <br/><a href="{{ url($planAuditDetailObj->path2) }}" target="_blank"><img height="100px" src="{{ url($planAuditDetailObj->path2) }}" ></a>
                            @endif
                        @if (!empty($planAuditDetailObj->path3))
                            <br/><a href="{{ url($planAuditDetailObj->path3) }}" target="_blank"><img height="100px" src="{{ url($planAuditDetailObj->path3) }}" ></a>
                        @endif
                        @if (!empty($planAuditDetailObj->path4))
                            <br/><a href="{{ url($planAuditDetailObj->path4) }}" target="_blank"><img height="100px" src="{{ url($planAuditDetailObj->path4) }}" ></a>
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

  @php
         $OldDate = new DateTime($planAuditDetailObj->sowing->start_date);
    $now = new DateTime(Date('Y-m-d'));
    $result = $OldDate->diff($now);
// echo $result->days;
    @endphp
    <hr/>
GE=ระดับความเสียหายน้อย, ET=ระดับความเสียหายปานกลาง, EL=ระดับความเสียหายวิกฤต
    <div class="table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th> 
                    <th>Cases</th>
                    <th>
                        @if (!empty($planAuditDetailObj->lat1))
                            <a href="http://www.google.com/maps/place/{{ $planAuditDetailObj->lat1 }},{{ $planAuditDetailObj->lng1 }}" target='_blank'>Point 1</a>
                        @else
                            Point 1
                        @endif
                    </th>
                    <th>
                        @if (!empty($planAuditDetailObj->lat2))
                            <a href="http://www.google.com/maps/place/{{ $planAuditDetailObj->lat2 }},{{ $planAuditDetailObj->lng2 }}" target='_blank'>Point 2</a>
                        @else
                            Point 2
                        @endif
                    </th>
                    <th>
                        @if (!empty($planAuditDetailObj->lat3))
                            <a href="http://www.google.com/maps/place/{{ $planAuditDetailObj->lat3 }},{{ $planAuditDetailObj->lng3 }}" target='_blank'>Point 3</a>
                        @else
                            Point 3
                        @endif
                    </th>
                    <th>
                        @if (!empty($planAuditDetailObj->lat4))
                            <a href="http://www.google.com/maps/place/{{ $planAuditDetailObj->lat4 }},{{ $planAuditDetailObj->lng4 }}" target='_blank'>Point 4</a>
                        @else
                            Point 4
                        @endif
                    </th>
                    <th>
                        @if (!empty($planAuditDetailObj->lat5))
                            <a href="http://www.google.com/maps/place/{{ $planAuditDetailObj->lat5 }},{{ $planAuditDetailObj->lng5 }}" target='_blank'>Point 5</a>
                        @else
                            Point 5
                        @endif
                    </th>
                    <th>รวมคะแนน</th>
                    <th>ระดับความเสียหายเนื่องจากศัตรูพืช</th>
                    <th>ระดับความเสียหาย</th>
                    <th>เกณฑ์ประเมิน</th>
                    <th>เกรดเกษตรกร</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($planAuditDetailObj->resultinsectdisease as $mainitem)
                <tr>
                    <td colspan="9">
                        <h4>No. {{ $mainitem->insectdisease->seq }} ประเภท: {{ $mainitem->insectdisease->type }} ชื่อ: {{ $mainitem->insectdisease->name }}</h4>
                            @if (!empty($mainitem->path1))
                                <a href="{{ url($mainitem->path1) }}" target="_blank"><img height="100px" src="{{ url($mainitem->path1) }}" ></a>
                             @endif
                             @if (!empty($mainitem->path2))
                                <a href="{{ url($mainitem->path2) }}" target="_blank"><img height="100px" src="{{ url($mainitem->path2) }}" ></a>
                            @endif
                        </td>
                        
                    <td style="vertical-align: middle;" rowspan="{{ $mainitem->resultdetailinsectdisease()->count()+1 }}" >
                        {{ $mainitem->result_text }}
                    </td>
                    <td style="vertical-align: middle;"  rowspan="{{ $mainitem->resultdetailinsectdisease()->count()+1 }}">
                        {{ $mainitem->result_desc }}
                    </td>
                    <td style="vertical-align: middle;" rowspan="{{ $mainitem->resultdetailinsectdisease()->count()+1 }}">
                        {{ $mainitem->result_grade }}
                    </td>
                        
                        
                </tr>
                
                @foreach ($mainitem->resultdetailinsectdisease as $item)
                    
                
                    <tr>
                        <td>{{ $item->insectdiseasecase->seq }}</td>
                        <td><span class="blockquote">{{ $item->insectdiseasecase->name }}</span> <br><span class="small">{{ $item->insectdiseasecase->result_desc }}</span></td>
                    <td>{{ $item->point1 }}</td>
                        <td>{{ $item->point2 }}</td>
                        <td>{{ $item->point3 }}</td>
                        <td> @if ($planAuditDetailObj->sowing->current_land < 3)
                                - 
                            @else
                                {{ $item->point4 }}
                            @endif 
                        </td>
                        <td>@if ($planAuditDetailObj->sowing->current_land < 3)
                                -
                            @else
                                {{ $item->point5 }}
                            @endif 
                        </td>
                        <td>{{ round($item->point_result,3) }}</td>
                        <td>{{ $item->txt_result }}</td>
                     
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="12">{{ $mainitem->note }}
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection