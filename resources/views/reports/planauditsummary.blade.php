@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="5">
                    Audit: {{ $PlanAudit->name }} ของวันที่ {{ $PlanAudit->plan_date }} ({{ $PlanAudit->status }})
<br/>{{ $PlanAudit->desc }} 
                </th>
            </tr>
            <tr>
                <th>A</th>
                <th>B</th>
                <th>C</th>
                <th>D</th>
                <th>ยังไม่ได้ตรวจ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $PlanAudit->planauditdetail()->where('result_grade','=','A')->count() }}</td>
                <td>{{ $PlanAudit->planauditdetail()->where('result_grade','=','B')->count() }}</td>
                <td>{{ $PlanAudit->planauditdetail()->where('result_grade','=','C')->count() }}</td>
                <td>{{ $PlanAudit->planauditdetail()->where('result_grade','=','D')->count() }}</td>
                <td>{{ $PlanAudit->planauditdetail()->where('result_grade','=','')->count() }}</td>
            </tr>
        </tbody>
    </table>
    <p>GE=ระดับความเสียหายน้อย, ET=ระดับความเสียหายปานกลาง, EL=ระดับความเสียหายวิกฤต</p>
    <table class="table table-bordered">
        <thead>
            
            <tr>
                <th colspan="4">Part.1 สรุปประเด็นไม่สอดคล้องที่พบ</th>
            </tr>
            <tr>
                <th>จุดควบคุม</th>
                <th>ระดับ CAR</th>
                <th>รายชื่อเกษตรกรที่พบ</th>
                <th>รายละเอียด</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($mainquestions as $mainquestion)
            @php
                $loops = 0;
                $numofmage = sizeof($data[$mainquestion->id]); 
            @endphp
            @foreach ($data[$mainquestion->id] as $subitem)
            <tr>
                @if ($loops == 0)
                    <td rowspan="{{ $numofmage  }}" >
                        {{ $mainquestion->name }}
                    </td>
                    <td rowspan="{{ $numofmage  }}" >
                        {{ $mainquestion->typequestion->name }}
                    </td>  
                @else
                @endif
                <td>{{ $subitem->planauditdetail->sowing->userfarmer->farmer->fname }} {{ $subitem->planauditdetail->sowing->userfarmer->farmer->lname }} ปลูกวันที่ {{ $subitem->planauditdetail->sowing->start_date }} ขนาด {{ $subitem->planauditdetail->sowing->current_land }} ไร่</td>
                <td>{{ $subitem->note }}
                @if (!empty($subitem->path1))
                    <a href="{{ url($subitem->path1) }}" target="_blank"><img height="200px" src="{{ url($subitem->path1) }}" ></a>          
                @endif
                </td>
            </tr>
            @php
                $loops++;
            @endphp
            @endforeach
            @endforeach
        </tbody>
    </table>

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="3">Part.2 สรุปเกรดเกษตรกรและศัตรูพืชที่พบ</th>
            </tr>
            <tr>
                <th>ชื่อ</th>
                <th>เกรด</th>
                <th>ศัตรูพืชที่พบ</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dataInsect as $insectresult)
            <tr>
                <td>{{ $insectresult->planauditdetail->sowing->userfarmer->farmer->fname }} {{ $insectresult->planauditdetail->sowing->userfarmer->farmer->lname }}
                ปลูกวันที่ {{ $insectresult->planauditdetail->sowing->start_date }} ขนาด {{ $insectresult->planauditdetail->sowing->current_land }} ไร่ 
                </td>
                <td>{{ $insectresult->result_grade }}</td>
                <td>
                    @php
                        $loopbr = 0;
                    @endphp
                    @foreach ($insectresult->planauditdetail->resultinsectdisease as $item)
                        @if (!empty($item->result_desc))
                            @if ($loopbr > 0)
                                <br/>    
                            @endif
                                {{ $item->insectdisease->name }} ({{ $item->result_text }})
                                @if ($item->path1)
                                    
                                @endif
                                @if (!empty($item->path1))
                                    <br/><a href="{{ url($item->path1) }}" target="_blank"><img height="200px" src="{{ url($item->path1) }}" ></a>          
                                @endif
                                @if (!empty($item->path2))
                                    <br/><a href="{{ url($item->path2) }}" target="_blank"><img height="200px" src="{{ url($item->path2) }}" ></a>          
                                @endif
                            @php
                                $loopbr++;
                            @endphp    
                        @endif
                    @endforeach
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
</div>
@endsection