
<div class="col-md-4 {{ $errors->has('group_question_id') ? 'has-error' : ''}}">
        {!! Form::label('group_question_id', 'status', ['class' => 'control-label']) !!}
        @if (isset($MainQuestion->group_question_id))
            {!! Form::select('group_question_id', $qroupquestionlist,$MainQuestion->group_question_id, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('group_question_id', $qroupquestionlist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('group_question_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-1  {{ $errors->has('seq') ? 'has-error' : ''}}">
    <label for="seq" class="control-label">{{ 'seq' }}</label>
    <input class="form-control"  name="seq" type="number" id="seq" required value="{{ $MainQuestion->seq or ''}}" >
    {!! $errors->first('seq', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-7  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'name' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $MainQuestion->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $MainQuestion->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>