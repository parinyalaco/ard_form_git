@extends('backLayout.app')
@section('title')
MainQuestion
@stop

@section('content')

    <h1>MainQuestions <a href="{{ url('MainQuestions/create') }}" class="btn btn-primary pull-right btn-sm">Add New MainQuestion</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblMainQuestions">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Seq</th>
                    <th>Group</th>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($MainQuestions as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->seq }}</td>
                    <td>{{ $item->groupquestion->name }}</td>
                    <td>{{ $item->name }}</td>
                    
                    <td>
                        <a href="{{ url('MainQuestions/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['MainQuestions', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblMainQuestions').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection