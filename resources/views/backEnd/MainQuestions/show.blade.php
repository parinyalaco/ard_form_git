@extends('backLayout.app')
@section('title')
MainQuestion
@stop

@section('content')

    <h1>MainQuestion</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> 
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $MainQuestion->id }}</td> 
                </tr>
            </tbody>    
        </table>
    </div>

@endsection