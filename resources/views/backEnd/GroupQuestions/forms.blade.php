<div class="col-md-4  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'name' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $GroupQuestion->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
@php
    $statuslist = array(
        'Active' => 'Active',
        'InActive' => 'InActive'
    );
@endphp
<div class="col-md-4 {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', 'status', ['class' => 'control-label']) !!}
        @if (isset($GroupQuestion->status))
            {!! Form::select('status', $statuslist,$GroupQuestion->status, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('status', $statuslist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $GroupQuestion->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>