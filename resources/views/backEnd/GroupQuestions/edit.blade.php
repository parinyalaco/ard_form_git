@extends('backLayout.app')
@section('title')
Edit GroupQuestion
@stop

@section('content')

    <h1>Edit GroupQuestion</h1>
    <hr/>

    {!! Form::model($GroupQuestion, [
        'method' => 'PATCH',
        'url' => ['GroupQuestions', $GroupQuestion->id],
        'class' => 'form-horizontal'
    ]) !!}

    @include ('backend.GroupQuestions.forms')

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection