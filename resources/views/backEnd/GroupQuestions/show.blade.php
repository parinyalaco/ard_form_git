@extends('backLayout.app')
@section('title')
GroupQuestion
@stop

@section('content')

    <h1>GroupQuestion</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> 
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $GroupQuestion->id }}</td> 
                </tr>
            </tbody>    
        </table>
    </div>

@endsection