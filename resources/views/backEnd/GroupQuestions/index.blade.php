@extends('backLayout.app')
@section('title')
GroupQuestion
@stop

@section('content')

    <h1>GroupQuestions <a href="{{ url('GroupQuestions/create') }}" class="btn btn-primary pull-right btn-sm">Add New GroupQuestion</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblGroupQuestions">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Staus</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($GroupQuestions as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->status }}</td>
                    <td>
                        <a href="{{ url('GroupQuestions/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['GroupQuestions', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblGroupQuestions').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection