@php
    $statuslist = array(
    'Active' => 'Active',
    'Inactive' => 'Inactive',
);
@endphp
<div class="col-md-4 {{ $errors->has('group_question_id') ? 'has-error' : ''}}">
        {!! Form::label('group_question_id', 'Group Question', ['class' => 'control-label']) !!}
        @if (isset($PlanAudit->main_question_id))
            {!! Form::select('group_question_id', $groupquestionlist,$PlanAudit->group_question_id, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('group_question_id', $groupquestionlist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('group_question_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('set_insect_disease_id') ? 'has-error' : ''}}">
        {!! Form::label('set_insect_disease_id', 'Insect Disease', ['class' => 'control-label']) !!}
        @if (isset($PlanAudit->set_insect_disease_id))
            {!! Form::select('set_insect_disease_id', $setinsectdiseaselist,$PlanAudit->set_insect_disease_id, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('set_insect_disease_id', $setinsectdiseaselist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('set_insect_disease_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('plan_date') ? 'has-error' : ''}}">
    <label for="plan_date" class="control-label">{{ 'Plan Date' }}</label>
    <input class="form-control"  name="plan_date" type="date" id="plan_date" required value="{{ $PlanAudit->plan_date or ''}}" >
    {!! $errors->first('plan_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'name' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $PlanAudit->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', 'Group Question', ['class' => 'control-label']) !!}
        @if (isset($PlanAudit->status))
            {!! Form::select('status', $statuslist,$PlanAudit->status, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('status', $statuslist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $PlanAudit->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>