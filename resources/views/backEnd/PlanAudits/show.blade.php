@extends('layouts.app')
@section('title')
PlanAudit
@stop

@section('content')
@php
    $user = Auth::user();
    $groupname = $user->group->name;
@endphp
<div class="container">



    <div class="row">


<h1>หน้าจัดการ Audit {{ $PlanAudit->name }} ของวันที่ {{ $PlanAudit->plan_date }} ({{ $PlanAudit->status }})</h1>
<h3>{{ $PlanAudit->desc }} 
    @if ($groupname == 'Admin' || $groupname == 'Auditreviewer' )
        <a href="{{ url('Reports/sendReport/'.$PlanAudit->id) }}" class="pull-right" style="padding-left: 10px;" onclick="return confirm('ยืนยันการส่งรายงานทาง Email?')"><img src="{{ url('/img/email.png') }}"></a>&nbsp;
    @endif
    <a href="{{ url('PlanAudits/reportPage/'.$PlanAudit->id) }}" class="pull-right" style="padding-left: 10px;"><img src="{{ url('/img/excel.png') }}"></a>&nbsp;
    <a href="{{ url('Reports/reportPage/'.$PlanAudit->id) }}" class="pull-right" style="padding-left: 10px;"><img src="{{ url('/img/news.png') }}"></a>
</h3>

<h1>รายการลูกสวนที่เลือกมาตรวจสอบ</h1>
<div class="table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr> <th>Crop</th> 
                    <th>Broker</th> 
                    <th>Farmer</th> 
                    <th>พันธุ์</th> 
                    <th>วันปลูก</th>
                    <th>อายุ</th>
                    <th>สภาพ</th>
                    <th>ครั้งที่</th>
                    <th>พื้นที่</th> 
                    <th>ผล</th> 
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $now = new DateTime($PlanAudit->plan_date);
                @endphp
                @foreach ($PlanAudit->planauditdetail as $item)
                  <tr>
                        <td>{{ $item->sowing->crop->name }}</td> 
                        <td>{{ $item->sowing->userfarmer->broker->code }}</td> 
                        <td>{{ $item->sowing->userfarmer->farmer->fname }} {{ $item->sowing->userfarmer->farmer->lname }}</td> 
                        <td>{{ $item->sowing->InputItem->tradename}}</td> 
                        <td>{{ $item->sowing->start_date}}</td>  
                        <td>
                            @php
                                $OldDate = new DateTime($item->sowing->start_date);
                                $result = $OldDate->diff($now);
                                echo $result->days;
                            @endphp
                        </td>
                        <td>{{ $item->sowing->harvest_status}}</td>
                        <td>{{ $item->seq }}</td>
                        <td>{{ $item->sowing->current_land }}
                            @if (!empty($item->lat) && !empty($item->lng))
                                <a href="http://www.google.com/maps/place/{{$item->lat}},{{$item->lng}}" target="_blank">
                                <img src="{{url('/img/map.png')}}" alt="Image"/></a>
                            @endif
                        </td> 
                        <td>
                            @if (!empty($item->result_txt))
                                {{ $item->result_txt }} <br/>
                                เกรด {{ $item->result_grade }} / 
                                จากผล {{ $item->result_num }}/{{ $item->auditresult()->count() }} ข้อ
                            @else
                                ยังไม่ได้ตรวจสอบ
                            @endif
                        </td>
                        <td>
                            <a href="{{ url('/Reports/reportByFarmerByAudit/'.$item->id) }}" target="_blank" ><img src="{{url('/img/report.png')}}" alt="Image"/> </a> 
                            @if ($item->status == 'lock')
                                    
                                @if ($groupname == 'Admin' || $groupname == 'Auditreviewer' )
                                    <a href="{{ url('/PlanAudits/updatedetailstatus/'.$item->id.'/active') }}" onclick="return confirm('Confirm for Unlock farmer?');"><img src="{{url('/img/unlock.png')}}" alt="Image"/></a>    
                                @endif
                            @else
                                <a href="{{ url('/PlanAudits/updateAuditForms/'.$item->id) }}" ><img src="{{url('/img/forms.png')}}" alt="Image"/> </a>
                                @if ($groupname == 'Admin' || $groupname == 'Auditreviewer' )
                                    <a href="{{ url('/PlanAudits/removedetail/'.$item->id) }}" onclick="return confirm('Confirm for Delete farmer?');"><img src="{{url('/img/delete.png')}}" alt="Image"/></a>
                                    <a href="{{ url('/PlanAudits/updatedetailstatus/'.$item->id.'/lock') }}" onclick="return confirm('Confirm for Lock farmer?');"><img src="{{url('/img/lock.png')}}" alt="Image"/></a>
                                @endif
                            @endif
                        </td>
                    </tr>  
                @endforeach
            </tbody>    
        </table>
        <h1>ค้นหาลูกสวน</h1>
        <form method="GET" action="{{ url('/PlanAudits/'.$PlanAudit->id) }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
            <div class="col-md-4">Farmer : <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}"></div>
            <div class="col-md-4">Crop : 
                @if (request('crop_id') != '')
                    {!! Form::select('crop_id', $croplist,request('crop_id'), ['class' => 'form-control caldate getorderlist getprice']) !!}   
                @else
                    {!! Form::select('crop_id', $croplist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
                @endif
            </div>
            <div class="col-md-4">Broker : 
                @if (request('broker_id') != '')
                    {!! Form::select('broker_id', $brokerlist,request('broker_id'), ['class' => 'form-control caldate getorderlist getprice']) !!}   
                @else
                    {!! Form::select('broker_id', $brokerlist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
                @endif
            </div>
            <div class="col-md-4">From : 
                <input class="form-control"  name="from_date" type="date" id="from_date" 
                @if (!empty(request('from_date')))
                    value="{{ date('Y-m-d',strtotime(request('from_date'))) }}"   
                @else
                    value="{{ date('Y-m-d') }}"
                @endif >
            </div>
                <div class="col-md-4">To : 
                <input class="form-control"  name="to_date" type="date" id="to_date" 
                @if (!empty(request('to_date')))
                    value="{{ date('Y-m-d',strtotime(request('to_date'))) }}"   
                @else
                    value="{{ date('Y-m-d') }}"
                @endif
                >
                </div>
                <div class="col-md-4"><span class="input-group-append">
                    <button class="btn btn-secondary" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </span></div>
            
        </form>
     <div class="table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr> <th>Crop</th> 
                    <th>Broker</th> 
                    <th>Farmer</th> 
                    <th>พันธุ์</th> 
                    <th>วันปลูก</th>
                    <th>อายุ</th>
                    <th>สภาพ</th>
                    <th>พื้นที่</th> 
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($sowingdata as $item)
                  <tr>
                      <td>{{ $item->crop->name }}</td> 
                    <td>{{ $item->userfarmer->broker->code }}</td> 
                    <td>{{ $item->userfarmer->farmer->fname }} {{ $item->userfarmer->farmer->lname }}</td> 
                    <td>{{ $item->InputItem->tradename}}</td> 
                    <td>{{ $item->start_date}}</td> 
                    
                    <td>
                        @php
                            $OldDate = new DateTime($item->start_date);
    $result = $OldDate->diff($now);
    echo $result->days;
                        @endphp
                    </td>
                    <td>{{ $item->harvest_status}}</td> 
                    <td>{{ $item->current_land }} 
                        @if ($item->gpxfiles()->count() > 0)
                            <img src="{{url('/img/map.png')}}" alt="Image"/>
                        @endif
                        
                    </td> 
                    <td><a href="{{ url('PlanAudits/addAuditLand/'. $PlanAudit->id .'/' . $item->id  ) }}" class="btn btn-primary btn-xs">Add</a> 
                        </td>
                </tr>  
                @endforeach
                
            </tbody>    
        </table>
        <div class="pagination-wrapper">
        @if (isset($sowingdata) && $sowingdata != NULL)
            {!! $sowingdata->appends([
            'search' => Request::get('search'),
            'crop_id' => Request::get('crop_id'),
            'broker_id' => Request::get('broker_id'),
            'from_date' => Request::get('from_date'),
            'to_date' => Request::get('to_date')
            ])->render() !!} 
        @else
            
        @endif
         </div>
</div>
</div>
</div>
@endsection