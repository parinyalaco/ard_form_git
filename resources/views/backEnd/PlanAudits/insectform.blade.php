@extends('layouts.app')
@section('title')
Create new PlanAudit
@stop

@section('content')
<div class="container">



    <div class="row">

<h1>{{ $planAuditDetailObj->sowing->userfarmer->broker->code }} / {{ $planAuditDetailObj->sowing->userfarmer->farmer->fname }} {{ $planAuditDetailObj->sowing->userfarmer->farmer->lname }}</h1>
<h2>{{ $planAuditDetailObj->sowing->inputitem->tradename }} ปลูกวันที่ {{ $planAuditDetailObj->sowing->start_date }} พื้นที่ {{ $planAuditDetailObj->sowing->current_land }} ไร่ อายุ
    @php
         $OldDate = new DateTime($planAuditDetailObj->sowing->start_date);
    $now = new DateTime(Date('Y-m-d'));
    $result = $OldDate->diff($now);
    echo $result->days;
    @endphp
    วัน <a href="{{ url('PlanAudits/updateAuditForms/'.$planAuditDetailObj->id) }}" class="btn btn-default pull-right btn-sm">Back</a></h2>
     
    <hr/>
    {!! Form::open(['url' => 'PlanAudits/updateInsectFormsAction/'.$plan_audit_detail_id.'/'.$audit_result_id, 'class' => 'form-horizontal', 'files' => true]) !!}

    <div class="table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th> 
                    <th>Cases</th>
                    <th>Point 1 
                    @if (!empty($planAuditDetailObj->lat1))
                       <a href="{{url('PlanAudits/removegps/'.$plan_audit_detail_id.'/'.$audit_result_id.'/1')}}" id="rgps1" class=" btn btn-primary btn-xs">remove GPS 1</a> 
                    @else
                       <a href="#" id="gps1" class=" btn btn-primary btn-xs">Get GPS 1</a>  
                    @endif  
                    </th>
                    <th>Point 2 @if (!empty($planAuditDetailObj->lat2))
                       <a href="{{url('PlanAudits/removegps/'.$plan_audit_detail_id.'/'.$audit_result_id.'/2')}}" id="rgps2" class=" btn btn-primary btn-xs">remove GPS 2</a> 
                    @else
                       <a href="#" id="gps2" class=" btn btn-primary btn-xs">Get GPS 2</a>  
                    @endif  
                    </th>
                    </th>
                    <th>Point 3 @if (!empty($planAuditDetailObj->lat3))
                       <a href="{{url('PlanAudits/removegps/'.$plan_audit_detail_id.'/'.$audit_result_id.'/3')}}" id="rgps3" class=" btn btn-primary btn-xs">remove GPS 3</a> 
                    @else
                       <a href="#" id="gps3" class=" btn btn-primary btn-xs">Get GPS 3</a>  
                    @endif  
                    </th>
                    <th>Point 4 @if (!empty($planAuditDetailObj->lat4))
                       <a href="{{url('PlanAudits/removegps/'.$plan_audit_detail_id.'/'.$audit_result_id.'/4')}}" id="rgps4" class=" btn btn-primary btn-xs">remove GPS 4</a> 
                    @else
                       <a href="#" id="gps4" class=" btn btn-primary btn-xs">Get GPS 4</a>  
                    @endif  
                    </th>
                    <th>Point 5 @if (!empty($planAuditDetailObj->lat5))
                       <a href="{{url('PlanAudits/removegps/'.$plan_audit_detail_id.'/'.$audit_result_id.'/3')}}" id="rgps3" class=" btn btn-primary btn-xs">remove GPS 5</a> 
                    @else
                       <a href="#" id="gps5" class=" btn btn-primary btn-xs">Get GPS 5</a>  
                    @endif  
                    </th>
                    <th>รวมคะแนน</th>
                    <th>ระดับความเสียหายเนื่องจากศัตรูพืช</th>
                    <th>ระดับความเสียหาย</th>
                    <th>เกณฑ์ประเมิน</th>
                    <th>เกรดเกษตรกร</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($planAuditDetailObj->resultinsectdisease as $mainitem)
                <tr>
                    <td colspan="9">
                        <h4>No. {{ $mainitem->insectdisease->seq }} ประเภท: {{ $mainitem->insectdisease->type }} ชื่อ: {{ $mainitem->insectdisease->name }}</h4>
                            {!! Form::file('path1_'.$mainitem->id, $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!} 
                            @if (!empty($mainitem->path1))
                                <a href="{{ url($mainitem->path1) }}" target="_blank"><img height="20px" src="{{ url($mainitem->path1) }}" ></a>
                                {!! Form::checkbox('remove_path1_'.$mainitem->id,'remove',false); !!} remove
                            @endif
                            {!! Form::file('path2_'.$mainitem->id, $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}
                            @if (!empty($mainitem->path2))
                                <a href="{{ url($mainitem->path2) }}" target="_blank"><img height="20px" src="{{ url($mainitem->path2) }}" ></a>
                                {!! Form::checkbox('remove_path2_'.$mainitem->id,'remove',false); !!} remove
                            @endif
                        </td>
                        
                    <td style="vertical-align: middle;" rowspan="{{ $mainitem->resultdetailinsectdisease()->count()+1 }}" >
                        <input class="form-control" name="allresult_txt_{{ $mainitem->id }}" type="text" id="allresult_txt_{{ $mainitem->id }}" readonly value="{{ $mainitem->result_text }}" >
                    </td>
                    <td style="vertical-align: middle;"  rowspan="{{ $mainitem->resultdetailinsectdisease()->count()+1 }}">
                        <input class="form-control" name="allresult_case_{{ $mainitem->id }}" type="text" id="allresult_case_{{ $mainitem->id }}" readonly value="{{ $mainitem->result_desc }}" >
                    </td>
                    <td style="vertical-align: middle;" rowspan="{{ $mainitem->resultdetailinsectdisease()->count()+1 }}">
                        <input class="form-control" name="allresult_grade_{{ $mainitem->id }}" type="text" id="allresult_grade_{{ $mainitem->id }}" readonly value="{{ $mainitem->result_grade }}" >
                    </td>
                        
                        
                </tr>
                
                @foreach ($mainitem->resultdetailinsectdisease as $item)
                    
                
                    <tr>
                        <td>{{ $item->insectdiseasecase->seq }}</td>
                        <td>{{ $item->insectdiseasecase->name }}</td>
                    <td><input class="form-control formular{{ $item->id }}"  name="point1_{{ $item->id }}" type="number" id="point1_{{ $item->id }}"  value="{{ $item->point1 }}" ></td>
                        <td><input class="form-control formular{{ $item->id }}"  name="point2_{{ $item->id }}" type="number" id="point2_{{ $item->id }}"  value="{{ $item->point2 }}" ></td>
                        <td><input class="form-control formular{{ $item->id }}"  name="point3_{{ $item->id }}" type="number" id="point3_{{ $item->id }}"  value="{{ $item->point3 }}" ></td>
                        <td>
                            <input class="form-control formular{{ $item->id }}"  name="point4_{{ $item->id }}" type="number" id="point4_{{ $item->id }}" 
                            @if ($planAuditDetailObj->sowing->current_land < 3)
                                readonly
                            @endif 
                            value="{{ $item->point4 }}" >
                        </td>
                        <td><input class="form-control formular{{ $item->id }}"  name="point5_{{ $item->id }}" type="number" id="point5_{{ $item->id }}" 
                            @if ($planAuditDetailObj->sowing->current_land < 3)
                                readonly
                            @endif 
                             value="{{ $item->point5 }}" >
                        </td>
                        <td><input class="form-control result{{ $item->id }}"  name="result_{{ $item->id }}" type="text" id="result_{{ $item->id }}" readonly value="{{ $item->point_result }}" ></td>
                        <td><input class="form-control maintxt{{ $mainitem->id }} result_txt{{ $item->id }}"  name="result_txt_{{ $item->id }}" type="text" id="result_txt_{{ $item->id }}" readonly value="{{ $item->txt_result }}" ></td>
                     
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="12"><input class="form-control" name="note{{ $mainitem->id }}" type="text" id="note{{ $mainitem->id }}" value="{{ $mainitem->note }}" >
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        
    </div>

    <div class="form-group">
        <div class="col-sm-3">
            <input name="noissues" type="checkbox" id="noissues" value="yes" ><label for="noissues">ไม่พบการระบาดใดๆ</label> 
        </div>
        <div class="col-sm-3">
            {!! Form::submit('บันทึก', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    </div>

</div>
    <script>
        $(document).ready(function() {
            @foreach ($planAuditDetailObj->resultinsectdisease as $mainitem)
                @foreach ($mainitem->resultdetailinsectdisease as $item)
                    $( ".formular{{ $item->id }}" ).change(function() {
                        //alert( "Handler for .change() called." );
                        var sum = 0;
                        var result = 0;
                        $('.formular{{ $item->id }}').each(function(){
                            if($(this).val() != ''){
                                sum += parseFloat($(this).val());  // Or this.innerHTML, this.innerText
                            }
                            
                        });
                        @if ($planAuditDetailObj->sowing->current_land >= 3) 
                            @if ($item->insectdiseasecase->method == 'Average')
                                result = sum/5;
                            @else
                                result = sum;
                            @endif 
                        @else 
                            @if ($item->insectdiseasecase->method == 'Average')
                                result = sum/3;
                            @else
                                result = sum;
                            @endif 
                        @endif
                        $('#result_{{$item->id}}').val(result);
                        @php
                            $func = explode(",",$item->insectdiseasecase->conds);
                           // var_dump($func);
                           echo "if(".$func[0]."){ result_txt='".$func[1]."'}else{if(".$func[2]."){ result_txt='".$func[3]."'}else{ result_txt='".$func[4]."'}}";
                        @endphp
                        $('#result_txt_{{$item->id}}').val(result_txt);
                        _recal{{ $mainitem->id }}();
                    });
                @endforeach

                function _recal{{ $mainitem->id }}(){
                    var mainratetxt = 'GE';
                    var maintxtcase = '';
                    var maintxtgrade = '';
                    $('.maintxt{{ $mainitem->id }}').each(function(){
                        if($(this).val() == ''){

                        }else{
                            if($(this).val() == 'EL'){
                                mainratetxt = 'EL';
                            }else{
                                if($(this).val() == 'ET'){
                                    if(mainratetxt == 'ET' || mainratetxt == 'GE'){
                                        mainratetxt = 'ET';
                                    }
                                }else{
                                    if(mainratetxt == 'GE'){
                                        mainratetxt = 'GE';
                                    }
                                }    
                            }
                        }
                    });
                    switch (mainratetxt){
                    @php
                        $casestep2 = explode(",",$mainitem->insectdisease->result_txts);
                       // var_dump($casestep2);
                        foreach($casestep2 as $key=>$caseobj){
                            $subcase = explode(":",$caseobj);
                        //    var_dump($subcase);
                            echo "case '".$subcase[0]."': maintxtcase = '".$subcase[1]."'; break; ";
                        }
                    @endphp
                    }
                    $('#allresult_txt_{{$mainitem->id}}').val(mainratetxt);
                    $('#allresult_case_{{$mainitem->id}}').val(maintxtcase);
                    switch (maintxtcase){
                    @php
                        $casestep2 = explode(",",$mainitem->insectdisease->grades);
                       // var_dump($casestep2);
                        foreach($casestep2 as $key=>$caseobj){
                            $subcase = explode(":",$caseobj);
                        //    var_dump($subcase);
                            echo "case '".$subcase[0]."': maintxtgrade = '".$subcase[1]."'; break; ";
                        }
                    @endphp
                    }
                    $('#allresult_grade_{{$mainitem->id}}').val(maintxtgrade);
                }

            @endforeach
                @for ($i = 1; $i < 6; $i++)
            $("#gps{{$i}}").click(function(){
                getLocation{{$i}}();
            });

             @endfor
        });

    @for ($i = 1; $i < 6; $i++)
function getLocation{{$i}}() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition{{$i}});
  } else { 
    alert("Geolocation is not supported by this browser.");
    return false;
  }
}

function showPosition{{$i}}(position) {
 alert("{{$i}} Latitude: " + position.coords.latitude + 
  "<br>Longitude: " + position.coords.longitude);
  window.location.href = "{{url('/PlanAudits/updategps/'.$plan_audit_detail_id.'/'.$audit_result_id.'/'.$i)}}/" + position.coords.latitude + '/' +position.coords.longitude ;
}        
    @endfor


    </script>

@endsection