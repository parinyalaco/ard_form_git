@extends('layouts.app')
@section('title')
Create new PlanAudit
@stop

@section('content')
<div class="container">



    <div class="row">

<h1>{{ $planAuditDetailObj->sowing->userfarmer->broker->code }} / {{ $planAuditDetailObj->sowing->userfarmer->farmer->fname }} {{ $planAuditDetailObj->sowing->userfarmer->farmer->lname }}</h1>
<h2>{{ $planAuditDetailObj->sowing->inputitem->tradename }} ปลูกวันที่ {{ $planAuditDetailObj->sowing->start_date }} พื้นที่ {{ $planAuditDetailObj->sowing->current_land }} ไร่ อายุ
    <a href="{{ url('PlanAudits/'.$planAuditDetailObj->plan_audit_id) }}" class="btn btn-default pull-right btn-sm">Back</a>
    
   @php
         $OldDate = new DateTime($planAuditDetailObj->sowing->start_date);
    $now = new DateTime(Date('Y-m-d'));
    $result = $OldDate->diff($now);
    echo $result->days;
    @endphp
    วัน</h2>
    <hr/>

    {!! Form::open(['url' => 'PlanAudits/updateAuditFormsAction/'.$plan_audit_detail_id, 'class' => 'form-horizontal', 'files' => true]) !!}

    
    <div class="table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th width="50%">Name</th> 
                    <th>Type</th> 
                    <th width="15%">Answer</th>
                    <th>Note</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $maintext = "";
                @endphp
                @foreach ($planAuditDetailObj->auditresult()->get() as $item)
                    @if ($maintext <> $item->detailquestion->mainquestion->name )
                        @php
                            $maintext = $item->detailquestion->mainquestion->name
                        @endphp
                        <tr>
                        <td colspan="4"><b>{{ $maintext }}</b></td>
                        </tr>                        
                    @endif
                    <tr>
                    <td>
                    @if (trim($item->detailquestion->link) == 'Y')
                    <a href="{{ url('/PlanAudits/updateInsectForms/'.$plan_audit_detail_id."/".$item->id) }}">{{ $item->detailquestion->name }}</a>
                    @else
                        {{ $item->detailquestion->name }}
                    @endif                    
                    </td>
                    <td>{{ $item->detailquestion->typequestion->name }}</td>    
                    <td>
                        @if (trim($item->detailquestion->link) == 'Y')
                            {{ $item->result_txt }}
                        @else
                            @if (isset($item->result_id))
                                {!! Form::select('answer_id_'.$item->id, $answerset[$item->detailquestion->set_ans_question_id],$item->result_id, ['class' => 'form-control caldate getorderlist getprice','placeholder' => '=เลือก=']) !!}   
                            @else
                                {!! Form::select('answer_id_'.$item->id, $answerset[$item->detailquestion->set_ans_question_id],null, ['class' => 'form-control caldate getorderlist  getprice','placeholder' => '=เลือก=']) !!}
                            @endif
                        @endif  

                    </td>
                    <td>
                        @if (trim($item->detailquestion->link) != 'Y')
                            {!! Form::file('path1_'.$item->id, $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}
                            @if (!empty($item->path1))
                                <a href="{{ url($item->path1) }}" target="_blank"><img height="20px" src="{{ url($item->path1) }}" ></a>
                                {!! Form::checkbox('remove_'.$item->id,'remove',false); !!} remove
                            @endif
                        @endif
                    <input class="form-control" name="note_{{$item->id}}" type="text" id="note_{{$item->id}}" value="{{ $item->note or ''}}" >
                    </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4">
                        {!! Form::file('path1', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}
                        @if (!empty($planAuditDetailObj->path1))
                            <a href="{{ url($planAuditDetailObj->path1) }}" target="_blank"><img height="20px" src="{{ url($planAuditDetailObj->path1) }}" ></a>
                            {!! Form::checkbox('removepath1','remove',false); !!} remove
                        @endif
                        {!! Form::file('path2', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}
                        @if (!empty($planAuditDetailObj->path2))
                            <a href="{{ url($planAuditDetailObj->path2) }}" target="_blank"><img height="20px" src="{{ url($planAuditDetailObj->path2) }}" ></a>
                            {!! Form::checkbox('removepath2','remove',false); !!} remove
                        @endif
                        {!! Form::file('path3', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}
                        @if (!empty($planAuditDetailObj->path3))
                            <a href="{{ url($planAuditDetailObj->path3) }}" target="_blank"><img height="20px" src="{{ url($planAuditDetailObj->path3) }}" ></a>
                            {!! Form::checkbox('removepath3','remove',false); !!} remove
                        @endif
                        {!! Form::file('path4', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}
                        @if (!empty($planAuditDetailObj->path4))
                            <a href="{{ url($planAuditDetailObj->path4) }}" target="_blank"><img height="20px" src="{{ url($planAuditDetailObj->path4) }}" ></a>
                            {!! Form::checkbox('removepath4','remove',false); !!} remove
                        @endif
                        <input class="form-control" name="note" type="text" id="note" value="{{ $planAuditDetailObj->note or ''}}" >
                    
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="form-group">
        <div class="col-sm-3">
            {!! Form::submit('บันทึก', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    </div>
</div>
@endsection