@extends('layouts.app')
@section('title')
PlanAudit
@stop

@section('content')
@php
    $user = Auth::user();
    $groupname = $user->group->name;
@endphp    
<div class="container">



    <div class="row">
    <h1>แผนการตรวจไร่ ARD <a href="{{ url('PlanAudits/create') }}" class="btn btn-primary pull-right btn-sm">สร้างแผน</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblPlanAudits">
            <thead>
                <tr>
                    <th>รหัส</th>
                    <th>วันที่ Audit</th>
                    <th>ชื่อ</th>
                    <th>จำนวนแปลงที่ Audit แล้ว/จำนวนแปลงทั้งหมด </th>
                    <th>A</th>
                    <th>B</th>
                    <th>C</th>
                    <th>D</th>
                    <th>ไม่ได้ตรวจ</th>
                    <th>สถานะ</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($PlanAudits as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->plan_date }}</td>
                    <th>{{ $item->name }}</td>
                    <td>{{ $item->planauditdetail()->where('result_txt','!=','')->count() }}  / {{ $item->planauditdetail()->count() }} </th>
                    <td>{{ $item->planauditdetail()->where('result_grade','=','A')->count() }}</td>
                    <td>{{ $item->planauditdetail()->where('result_grade','=','B')->count() }}</td>
                    <td>{{ $item->planauditdetail()->where('result_grade','=','C')->count() }}</td>
                    <td>{{ $item->planauditdetail()->where('result_grade','=','D')->count() }}</td>
                    <td>{{ $item->planauditdetail()->where('result_txt','=','')->count() }}</td>
                    <td>{{ $item->status }}</td>
                    <td>
                        <a href="{{ url('PlanAudits/' . $item->id ) }}" class="btn btn-primary btn-xs">จัดการ</a> 
                         @if ($groupname == 'Admin' || $groupname == 'Auditreviewer' )
                            <a href="{{ url('PlanAudits/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">แก้ไข</a> 
                            @if ($item->status == 'Active')
                                <a href="{{ url('PlanAudits/updatestatus/' . $item->id .'/Done') }}" class="btn btn-primary btn-xs">Done</a> 
                                {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['PlanAudits', $item->id],
                                'style' => 'display:inline'
                                    
                                ]) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs','onclick'=>'return confirm("Confirm delete?")']) !!}
                                {!! Form::close() !!}
                            @else
                                <a href="{{ url('PlanAudits/updatestatus/' . $item->id.'/Active' ) }}" class="btn btn-primary btn-xs">Active</a> 
                            @endif
                            
                            
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination-wrapper"> {!! $PlanAudits->render() !!} </div>
    </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblPlanAudits').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection