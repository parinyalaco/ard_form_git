@extends('layouts.app')
@section('title')
Edit PlanAudit
@stop

@section('content')
<div class="container">



    <div class="row">
    <h1>แก้ไขแผน Audit</h1>
    <hr/>

    {!! Form::model($PlanAudit, [
        'method' => 'PATCH',
        'url' => ['PlanAudits', $PlanAudit->id],
        'class' => 'form-horizontal'
    ]) !!}

    @include ('backend.PlanAudits.forms')

    <div class="form-group">
        <div class="col-sm-3">
            {!! Form::submit('แก้ไข', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    </div>
</div>
@endsection