@extends('layouts.app')
@section('title')
Create new PlanAudit
@stop

@section('content')
<div class="container">



    <div class="row">
    <h1>สร้างแผน Audit</h1>
    <hr/>

    {!! Form::open(['url' => 'PlanAudits', 'class' => 'form-horizontal']) !!}

    @include ('backend.PlanAudits.forms')

    <div class="form-group">
        <div class="col-sm-3">
            {!! Form::submit('สร้าง', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    </div>
</div>
@endsection