
<div class="col-md-4  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'name' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $InsectDiseaseCase->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
@php
    $methodlist = array(
        'Sum' => 'Sum',
        'Average' => 'Avg'
    );
@endphp
<div class="col-md-3 {{ $errors->has('method') ? 'has-error' : ''}}">
        {!! Form::label('method', 'Method', ['class' => 'control-label']) !!}
        @if (isset($InsectDiseaseCase->status))
            {!! Form::select('method', $methodlist,$InsectDiseaseCase->method, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('method', $methodlist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('method', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-2  {{ $errors->has('seq') ? 'has-error' : ''}}">
    <label for="seq" class="control-label">{{ 'Seq' }}</label>
    <input class="form-control" name="seq" type="number" id="seq" required value="{{ $InsectDiseaseCase->seq or ''}}" >
    {!! $errors->first('seq', '<p class="help-block">:message</p>') !!}
</div>

@php
    $statuslist = array(
        'Active' => 'Active',
        'InActive' => 'InActive'
    );
@endphp
<div class="col-md-3 {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', 'status', ['class' => 'control-label']) !!}
        @if (isset($InsectDiseaseCase->status))
            {!! Form::select('status', $statuslist,$InsectDiseaseCase->status, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('status', $statuslist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $InsectDiseaseCase->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('conds') ? 'has-error' : ''}}">
    <label for="conds" class="control-label">{{ 'เงื่อนไข' }}</label>
    <textarea class="form-control" name="conds" type="text" id="conds">{{ $InsectDiseaseCase->conds or ''}}</textarea>
    {!! $errors->first('conds', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('result_text') ? 'has-error' : ''}}">
    <label for="result_text" class="control-label">{{ 'รายการระดับความเสียหายเนื่องจากศัตรูพืช' }}</label>
    <textarea class="form-control" name="result_text" type="text" id="result_text">{{ $InsectDiseaseCase->result_text or ''}}</textarea>
    {!! $errors->first('result_text', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('result_desc') ? 'has-error' : ''}}">
    <label for="result_desc" class="control-label">{{ 'รายการเกณฑ์ประเมิน' }}</label>
    <textarea class="form-control" name="result_desc" type="text" id="result_desc">{{ $InsectDiseaseCase->result_desc or ''}}</textarea>
    {!! $errors->first('result_desc', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('result_grade') ? 'has-error' : ''}}">
    <label for="result_grade" class="control-label">{{ 'รายการเกรด' }}</label>
    <textarea class="form-control" name="result_grade" type="text" id="result_grade">{{ $InsectDiseaseCase->result_grade or ''}}</textarea>
    {!! $errors->first('result_grade', '<p class="help-block">:message</p>') !!}
</div>

