<div class="col-md-4 {{ $errors->has('set_insect_disease_id') ? 'has-error' : ''}}">
        {!! Form::label('set_insect_disease_id', 'Set', ['class' => 'control-label']) !!}
        @if (isset($InsectDisease->set_insect_disease_id))
            {!! Form::select('set_insect_disease_id', $setInsectDiseaseList,$InsectDisease->set_insect_disease_id, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('set_insect_disease_id', $setInsectDiseaseList,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('set_insect_disease_id', '<p class="help-block">:message</p>') !!}
</div>
@php
    $typeList = array(
        'แมลง' => 'แมลง',
        'โรค' => 'โรค',
    );
@endphp
<div class="col-md-4 {{ $errors->has('type') ? 'has-error' : ''}}">
        {!! Form::label('type', 'Type', ['class' => 'control-label']) !!}
        @if (isset($InsectDisease->type))
            {!! Form::select('type', $typeList,$InsectDisease->type, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('type', $typeList,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'name' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $InsectDisease->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $InsectDisease->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('conds') ? 'has-error' : ''}}">
    <label for="conds" class="control-label">{{ 'เงื่อนไข' }}</label>
    <textarea class="form-control" name="conds" type="text" id="conds">{{ $InsectDisease->conds or ''}}</textarea>
    {!! $errors->first('conds', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('results') ? 'has-error' : ''}}">
    <label for="results" class="control-label">{{ 'รายการระดับความเสียหายเนื่องจากศัตรูพืช' }}</label>
    <textarea class="form-control" name="results" type="text" id="results">{{ $InsectDisease->results or ''}}</textarea>
    {!! $errors->first('results', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('result_txts') ? 'has-error' : ''}}">
    <label for="result_txts" class="control-label">{{ 'รายการเกณฑ์ประเมิน' }}</label>
    <textarea class="form-control" name="result_txts" type="text" id="result_txts">{{ $InsectDisease->result_txts or ''}}</textarea>
    {!! $errors->first('result_txts', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('grades') ? 'has-error' : ''}}">
    <label for="grades" class="control-label">{{ 'รายการเกรด' }}</label>
    <textarea class="form-control" name="grades" type="text" id="grades">{{ $InsectDisease->grades or ''}}</textarea>
    {!! $errors->first('grades', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-4  {{ $errors->has('seq') ? 'has-error' : ''}}">
    <label for="seq" class="control-label">{{ 'Seq' }}</label>
    <input class="form-control" name="seq" type="number" id="seq" required value="{{ $InsectDisease->seq or ''}}" >
    {!! $errors->first('seq', '<p class="help-block">:message</p>') !!}
</div>

@php
    $statuslist = array(
        'Active' => 'Active',
        'InActive' => 'InActive'
    );
@endphp
<div class="col-md-4 {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', 'status', ['class' => 'control-label']) !!}
        @if (isset($InsectDisease->status))
            {!! Form::select('status', $statuslist,$InsectDisease->status, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('status', $statuslist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>