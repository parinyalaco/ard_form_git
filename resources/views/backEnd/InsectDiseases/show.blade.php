@extends('backLayout.app')
@section('title')
InsectDisease
@stop

@section('content')

    <h1>รายการ {{ $InsectDisease->name }}</h1>
<h2>Set:{{ $InsectDisease->setinsectdisease->name }} / Type:{{ $InsectDisease->type }} / Seq:{{ $InsectDisease->seq }} / Status:{{ $InsectDisease->status }}</h2>
    
    <div class="table-responsive">
        
        <a href="{{ url('InsectDiseases') }}" class="btn btn-default pull-right btn-sm">Back</a>
        <a href="{{ url('InsectDiseases/createdetail/'.$InsectDisease->id) }}" class="btn btn-primary pull-right btn-sm">Add New InsectDisease</a>
    
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>seq.</th>
                    <th>Name</th>
                    <th>Method</th>
                    <th>desc</th>
                    <th>Status</th> 
                    <th>Action</th> 
                </tr>
            </thead>
            <tbody>
                @foreach ($InsectDisease->insectdiseasecase as $item)
                  <tr>
                    <td>{{ $item->seq }}</td> 
                    <td>{{ $item->name }}</td> 
                    <td>{{ $item->method }}</td> 
                    <td>{!! nl2br(e($item->desc)) !!}</td>
                    <td>{{ $item->status }}</td> 
                    <td>
                        <a href="{{ url('InsectDiseases/editdetail/' . $item->id ) }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['InsectDiseaseCases', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td> 
                </tr>  
                @endforeach
            </tbody>    
        </table>
    </div>

@endsection