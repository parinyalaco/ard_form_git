@extends('backLayout.app')
@section('title')
Create new InsectDisease
@stop

@section('content')


    <h1>Create New InsectDisease</h1>
        <h2>รายการ {{ $InsectDisease->name }}</h2>
<h3>Set:{{ $InsectDisease->setinsectdisease->name }} / Type:{{ $InsectDisease->type }} / Seq:{{ $InsectDisease->seq }} / Status:{{ $InsectDisease->status }}</h3>
    <a href="{{ url('InsectDiseases/'.$InsectDisease->id) }}" class="btn btn-default pull-right btn-sm">Back</a>
    <hr/>

    {!! Form::open(['url' => 'InsectDiseases/createdetailAction/'.$InsectDisease->id, 'class' => 'form-horizontal']) !!}

    @include ('backend.InsectDiseases.formsdetail')

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection