@extends('backLayout.app')
@section('title')
InsectDisease
@stop

@section('content')

    <h1>InsectDiseases <a href="{{ url('InsectDiseases/create') }}" class="btn btn-primary pull-right btn-sm">Add New InsectDisease</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblInsectDiseases">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Set</th>
                    <th>Seq</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Num of Cases</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($InsectDiseases as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->setinsectdisease->name or '' }}</td>
                    <td>{{ $item->seq }}</td>
                    <td>{{ $item->type }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->insectdiseasecase()->count() }}</td>
                    <td>{{ $item->status }}</td>
                    
                    <td>
                        <a href="{{ url('InsectDiseases/' . $item->id ) }}" class="btn btn-default btn-xs">Manage</a> 
                       
                        <a href="{{ url('InsectDiseases/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['InsectDiseases', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblInsectDiseases').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection