@extends('backLayout.app')
@section('title')
SetAnsQuestion
@stop

@section('content')

    <h1>SetAnsQuestions <a href="{{ url('SetAnsQuestions/create') }}" class="btn btn-primary pull-right btn-sm">Add New SetAnsQuestion</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblSetAnsQuestions">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($SetAnsQuestions as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>
                        <a href="{{ url('SetAnsQuestions/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['SetAnsQuestions', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblSetAnsQuestions').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection