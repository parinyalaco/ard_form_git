@extends('backLayout.app')
@section('title')
Edit SetAnsQuestion
@stop

@section('content')

    <h1>Edit SetAnsQuestion</h1>
    <hr/>

    {!! Form::model($SetAnsQuestion, [
        'method' => 'PATCH',
        'url' => ['SetAnsQuestions', $SetAnsQuestion->id],
        'class' => 'form-horizontal'
    ]) !!}

    @include ('backend.SetAnsQuestions.forms')

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection