@extends('backLayout.app')
@section('title')
SetAnsQuestion
@stop

@section('content')

    <h1>SetAnsQuestion</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> 
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $SetAnsQuestion->id }}</td> 
                </tr>
            </tbody>    
        </table>
    </div>

@endsection