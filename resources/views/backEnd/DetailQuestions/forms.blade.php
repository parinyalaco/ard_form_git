
<div class="col-md-4 {{ $errors->has('main_question_id') ? 'has-error' : ''}}">
        {!! Form::label('main_question_id', 'Main', ['class' => 'control-label']) !!}
        @if (isset($DetailQuestion->main_question_id))
            {!! Form::select('main_question_id', $mainquestionlist,$DetailQuestion->main_question_id, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('main_question_id', $mainquestionlist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('main_question_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('type_question_id') ? 'has-error' : ''}}">
        {!! Form::label('type_question_id', 'Type', ['class' => 'control-label']) !!}
        @if (isset($DetailQuestion->type_question_id))
            {!! Form::select('type_question_id', $typequestionlist,$DetailQuestion->type_question_id, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('type_question_id', $typequestionlist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('type_question_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-4 {{ $errors->has('set_ans_question_id') ? 'has-error' : ''}}">
        {!! Form::label('set_ans_question_id', 'Answer Set', ['class' => 'control-label']) !!}
        @if (isset($DetailQuestion->set_ans_question_id))
            {!! Form::select('set_ans_question_id', $setansquestionlist,$DetailQuestion->set_ans_question_id, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('set_ans_question_id', $setansquestionlist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('set_ans_question_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-1  {{ $errors->has('seq') ? 'has-error' : ''}}">
    <label for="seq" class="control-label">{{ 'seq' }}</label>
    <input class="form-control"  name="seq" type="number" id="seq" required value="{{ $DetailQuestion->seq or ''}}" >
    {!! $errors->first('seq', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-11  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'name' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $DetailQuestion->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $DetailQuestion->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>