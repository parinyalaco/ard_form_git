@extends('backLayout.app')
@section('title')
DetailQuestion
@stop

@section('content')

    <h1>DetailQuestion</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> 
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $DetailQuestion->id }}</td> 
                </tr>
            </tbody>    
        </table>
    </div>

@endsection