@extends('backLayout.app')
@section('title')
Edit DetailQuestion
@stop

@section('content')

    <h1>Edit DetailQuestion</h1>
    <hr/>

    {!! Form::model($DetailQuestion, [
        'method' => 'PATCH',
        'url' => ['DetailQuestions', $DetailQuestion->id],
        'class' => 'form-horizontal'
    ]) !!}

    @include ('backend.DetailQuestions.forms')

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection