@extends('backLayout.app')
@section('title')
DetailQuestion
@stop

@section('content')

    <h1>DetailQuestions <a href="{{ url('DetailQuestions/create') }}" class="btn btn-primary pull-right btn-sm">Add New DetailQuestion</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblDetailQuestions">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Main<br/>Name</th>
                    <th>Type</th>
                    <th>Set</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($DetailQuestions as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->mainquestion->name }}<br/>{{ $item->name }}</td>
                    <td>{{ $item->typequestion->name }}</td>
                    <td>{{ $item->setansquestion->name }}</td>
                    
                    <td>
                        <a href="{{ url('DetailQuestions/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['DetailQuestions', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblDetailQuestions').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection