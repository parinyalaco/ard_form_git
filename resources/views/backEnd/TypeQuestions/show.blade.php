@extends('backLayout.app')
@section('title')
TypeQuestion
@stop

@section('content')

    <h1>TypeQuestion</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> 
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $TypeQuestion->id }}</td> 
                </tr>
            </tbody>    
        </table>
    </div>

@endsection