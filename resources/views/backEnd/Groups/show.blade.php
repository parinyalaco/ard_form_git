@extends('backLayout.app')
@section('title')
Group
@stop

@section('content')

    <h1>Group</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> 
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $Group->id }}</td> 
                </tr>
            </tbody>    
        </table>
    </div>

@endsection