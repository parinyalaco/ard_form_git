@extends('backLayout.app')
@section('title')
Edit SetInsectDisease
@stop

@section('content')

    <h1>Edit SetInsectDisease</h1>
    <hr/>

    {!! Form::model($SetInsectDisease, [
        'method' => 'PATCH',
        'url' => ['SetInsectDiseases', $SetInsectDisease->id],
        'class' => 'form-horizontal'
    ]) !!}

    @include ('backend.SetInsectDiseases.forms')

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection