@extends('backLayout.app')
@section('title')
SetInsectDisease
@stop

@section('content')

    <h1>SetInsectDisease</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> 
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $SetInsectDisease->id }}</td> 
                </tr>
            </tbody>    
        </table>
    </div>

@endsection