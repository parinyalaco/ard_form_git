@extends('backLayout.app')
@section('title')
Create new SetInsectDisease
@stop

@section('content')

    <h1>Create New SetInsectDisease</h1>
    <hr/>

    {!! Form::open(['url' => 'SetInsectDiseases', 'class' => 'form-horizontal']) !!}

    @include ('backend.SetInsectDiseases.forms')

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection