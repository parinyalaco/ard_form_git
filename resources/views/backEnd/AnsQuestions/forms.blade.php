<div class="col-md-4  {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'name' }}</label>
    <input class="form-control" name="name" type="text" id="name" required value="{{ $AnsQuestion->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-4 {{ $errors->has('set_ans_question_id') ? 'has-error' : ''}}">
        {!! Form::label('set_ans_question_id', 'กลุ่ม', ['class' => 'control-label']) !!}
        @if (isset($AnsQuestion->set_ans_question_id))
            {!! Form::select('set_ans_question_id', $setansquestionlist,$AnsQuestion->set_ans_question_id, ['class' => 'form-control caldate getorderlist getprice']) !!}   
        @else
            {!! Form::select('set_ans_question_id', $setansquestionlist,null, ['class' => 'form-control caldate getorderlist  getprice']) !!}
        @endif
        {!! $errors->first('set_ans_question_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="col-md-12  {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <textarea class="form-control" name="desc" type="text" id="desc">{{ $AnsQuestion->desc or ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>