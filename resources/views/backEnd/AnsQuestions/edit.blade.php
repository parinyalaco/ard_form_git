@extends('backLayout.app')
@section('title')
Edit AnsQuestion
@stop

@section('content')

    <h1>Edit AnsQuestion</h1>
    <hr/>

    {!! Form::model($AnsQuestion, [
        'method' => 'PATCH',
        'url' => ['AnsQuestions', $AnsQuestion->id],
        'class' => 'form-horizontal'
    ]) !!}

    @include ('backend.AnsQuestions.forms')

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection