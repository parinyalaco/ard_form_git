@extends('backLayout.app')
@section('title')
AnsQuestion
@stop

@section('content')

    <h1>AnsQuestion</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> 
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $AnsQuestion->id }}</td> 
                </tr>
            </tbody>    
        </table>
    </div>

@endsection