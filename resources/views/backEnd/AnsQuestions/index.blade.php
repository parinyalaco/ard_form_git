@extends('backLayout.app')
@section('title')
AnsQuestion
@stop

@section('content')

    <h1>AnsQuestions <a href="{{ url('AnsQuestions/create') }}" class="btn btn-primary pull-right btn-sm">Add New AnsQuestion</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblAnsQuestions">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Label</th>
                    <th>Grade</th>
                    <th>Group</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($AnsQuestions as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->desc }}</td>
                    <td>{{ $item->setansquestion->name }}</td>
                    <td>
                        <a href="{{ url('AnsQuestions/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['AnsQuestions', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblAnsQuestions').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection