<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    tr th {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    tr th.noborder {
        border: none;
        word-wrap: normal;
    }
     tr th.noborder-last {
        border: none;
        word-wrap: normal;
    }
    tr th.noborderr {
        border: none;
        text-align: right;
        word-wrap: break-word;
    }
    tr th.noborderc {
        border: none;
        text-align: center;
        word-wrap: break-word;
        font: bolder;
    }
    tr td {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th colspan="6">Part.2 สรุปเกรดเกษตรกรและศัตรูพืชที่พบ</th>
            </tr>
            <tr>
                <th>ลำดับ</th>
                <th>รหัส/เขต</th>
                <th>ชื่อ</th>
                <th>อายุ</th>
                <th>เกรด</th>
                <th>ศัตรูพืชที่พบ</th>
            </tr>
        </thead>
        <tbody>
            @php
                $now = new DateTime($PlanAudit->plan_date);
            @endphp
            @foreach ($dataInsect as $insectresult)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $insectresult->planauditdetail->sowing->userfarmer->broker->code }} - {{ $insectresult->planauditdetail->sowing->userfarmer->broker->fname }}</td>
                <td>{{ $insectresult->planauditdetail->sowing->userfarmer->farmer->fname }} {{ $insectresult->planauditdetail->sowing->userfarmer->farmer->lname }}
                ปลูกวันที่ {{ $insectresult->planauditdetail->sowing->start_date }} ขนาด {{ $insectresult->planauditdetail->sowing->current_land }} ไร่ 
                </td>
                <td>@php
        $OldDate = new DateTime($insectresult->planauditdetail->sowing->start_date);
    $result = $OldDate->diff($now);
    echo $result->days;
                        @endphp</td>
                <td>{{ $insectresult->result_grade }}</td>
                <td>
                    @php
                        $loopbr = 0;
                    @endphp
                    @foreach ($insectresult->planauditdetail->resultinsectdisease as $item)
                        @if (!empty($item->result_desc))
                            @if ($loopbr > 0)
                                <br/>    
                            @endif
                                {{ $item->insectdisease->name }} ({{ $item->result_text }})
                            @php
                                $loopbr++;
                            @endphp    
                        @endif
                    @endforeach
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>