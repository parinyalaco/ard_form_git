<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    tr th {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    tr th.noborder {
        border: none;
        word-wrap: normal;
    }
     tr th.noborder-last {
        border: none;
        word-wrap: normal;
    }
    tr th.noborderr {
        border: none;
        text-align: right;
        word-wrap: break-word;
    }
    tr th.noborderc {
        border: none;
        text-align: center;
        word-wrap: break-word;
        font: bolder;
    }
    tr td {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th colspan="4">
                    Audit: {{ $PlanAudit->name }} ของวันที่ {{ $PlanAudit->plan_date }} ({{ $PlanAudit->status }})
<br/>{{ $PlanAudit->desc }} 

                </th>
            </tr>
            <tr>
                <th colspan="4">Part.1 สรุปประเด็นไม่สอดคล้องที่พบ</th>
            </tr>
            <tr>
                <th>ประเด็นที่ไม่สอดคล้อง</th>
                <th>ระดับ CAR</th>
                <th>รายชื่อเกษตรกรที่พบ</th>
                <th>รายละเอียด</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($mainquestions as $mainquestion)
            @php
                $loops = 0;
                $numofmage = sizeof($data[$mainquestion->id]); 
            @endphp
            @foreach ($data[$mainquestion->id] as $subitem)
            <tr>
                @if ($loops == 0)
                    <td rowspan="{{ $numofmage  }}" >
                        {{ $mainquestion->name }}
                    </td>
                    <td rowspan="{{ $numofmage  }}" >
                        {{ $mainquestion->typequestion->name }}
                    </td>  
                @else
                    <td></td>
                    <td></td>
                @endif
                <td>{{ $subitem->planauditdetail->sowing->userfarmer->farmer->fname }} {{ $subitem->planauditdetail->sowing->userfarmer->farmer->lname }} ปลูกวันที่ {{ $subitem->planauditdetail->sowing->start_date }} ขนาด {{ $subitem->planauditdetail->sowing->current_land }} ไร่</td>
                <td>{{ $subitem->note }}</td>
            </tr>
            @php
                $loops++;
            @endphp
            @endforeach
            @endforeach
        </tbody>
    </table>
</body>
</html>