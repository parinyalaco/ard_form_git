@extends('layouts.graph')

@section('content')
<div class="container">
    <div class='row'>
      @foreach ($croplist as $key=>$item)
    <a href="{{ url('/Dashboards/crop/'.$key) }}">[ {{$item }} ]</a> 
      @endforeach
    </div>
    <div class="row">
        <div id="chart_div" style=" height: 600px;"></div>
    </div>
</div>

<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);

      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
          ['Broker', 'จำนวนแปลงที่ Audit แล้ว', 'จำนวแปลงทั้งหมด', 'จำนวนแปลงที่ต้อง Audit (10%)'],
        @foreach ($dataset as $item)
        ['{{ $item->code }}',
        @if (isset($data[$item->broker_id]))
            {{ $data[$item->broker_id] }}
        @else
            0
        @endif
        ,{{ $item->countall }}
        ,{{ (0.1*$item->countall) }}
        ],
        @endforeach
        ]);

        var options = {
          title : 'Audit list ของ {{ $crop->name }}',
          vAxis: {title: 'จำนวนแปลง'},
          hAxis: {title: 'Brokers'},
          seriesType: 'bars',
          series: {2: {type: 'line'}}        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
@endsection