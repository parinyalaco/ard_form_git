<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ARD Reports Alert</title>
    <style>
        table {
            border: solid 2px black;
        }
        thead tr th{
            border: solid 1px black;
            font-weight: bold;
            font-style: italic;
        }
        tbody tr td{
            border: solid 1px black;
        }
    </style>
</head>
<body>
    @php
                $now = new DateTime($planaudit->plan_date);
            @endphp
    <p><strong>TO..ALL</strong></p>
    <p><strong>ARD Report Alert System</strong></p>
    <p>รายงานการ Audit ประจำวันที่ {{ $planaudit->plan_date }} {{ $planaudit->name }}</p>
    <p>สามารถไปดูรายงานได้ที่ File แนบ หรือ <a href="{{ url('/Reports/mainAudit/'.$plan_audit_id)}}">Click Link</a> ({{ url('/Reports/mainAudit/'.$plan_audit_id)}})
    </p>
    <p>GE=ระดับความเสียหายน้อย, ET=ระดับความเสียหายปานกลาง, EL=ระดับความเสียหายวิกฤต</p>
    <table style="width: 700px">
        <thead>
            <tr>
                <th colspan="4">
                    Audit: {{ $PlanAudit->name }} ของวันที่ {{ $PlanAudit->plan_date }} ({{ $PlanAudit->status }})
<br/>{{ $PlanAudit->desc }} 

                </th>
            </tr>
            <tr>
                <th colspan="4">Part.1 สรุปประเด็นไม่สอดคล้องที่พบ</th>
            </tr>
            <tr>
                <th>จุดควบคุม</th>
                <th>ระดับ CAR</th>
                <th>รายชื่อเกษตรกรที่พบ</th>
                <th>รายละเอียด</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($mainquestions as $mainquestion)
            @php
                $loops = 0;
                $numofmage = sizeof($data[$mainquestion->id]); 
            @endphp
            @foreach ($data[$mainquestion->id] as $subitem)
            <tr>
                @if ($loops == 0)
                    <td rowspan="{{ $numofmage  }}" >
                        {{ $mainquestion->name }}
                    </td>
                    <td rowspan="{{ $numofmage  }}" >
                        {{ $mainquestion->typequestion->name }}
                    </td>  
                @else
                @endif
                <td>{{ $subitem->planauditdetail->sowing->userfarmer->farmer->fname }} {{ $subitem->planauditdetail->sowing->userfarmer->farmer->lname }} ปลูกวันที่ {{ $subitem->planauditdetail->sowing->start_date }} ขนาด {{ $subitem->planauditdetail->sowing->current_land }} ไร่</td>
                <td>{{ $subitem->note }}
                @if (!empty($subitem->path1))
                    <a href="{{ url($subitem->path1) }}" target="_blank"><img height="200px" src="{{ url($subitem->path1) }}" ></a>          
                @endif
                </td>
            </tr>
            @php
                $loops++;
            @endphp
            @endforeach
            @endforeach
        </tbody>
    </table>
<br/>
    <table style="width: 700px">
        <thead>
            <tr>
                <th colspan="6">Part.2 สรุปเกรดเกษตรกรและศัตรูพืชที่พบ</th>
            </tr>
            <tr>
                <th>ลำดับ</th>
                <th>รหัส/เขต</th>
                <th>ชื่อ</th>
                <th>อายุ</th>
                <th>เกรด</th>
                <th>ศัตรูพืชที่พบ</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dataInsect as $insectresult)
            <tr><td>{{ $loop->iteration }}</td>
                <td>{{ $insectresult->planauditdetail->sowing->userfarmer->broker->code }} - {{ $insectresult->planauditdetail->sowing->userfarmer->broker->fname }}</td>
               
                <td>{{ $insectresult->planauditdetail->sowing->userfarmer->farmer->fname }} {{ $insectresult->planauditdetail->sowing->userfarmer->farmer->lname }}
                ปลูกวันที่ {{ $insectresult->planauditdetail->sowing->start_date }} ขนาด {{ $insectresult->planauditdetail->sowing->current_land }} ไร่ 
                </td>
                <td>@php
        $OldDate = new DateTime($insectresult->planauditdetail->sowing->start_date);
    $result = $OldDate->diff($now);
    echo $result->days;
                        @endphp</td>
                <td>{{ $insectresult->result_grade }}</td>
                <td>
                    @php
                        $loopbr = 0;
                    @endphp
                    @foreach ($insectresult->planauditdetail->resultinsectdisease as $item)
                        @if (!empty($item->result_desc))
                            @if ($loopbr > 0)
                                <br/>    
                            @endif
                                {{ $item->insectdisease->name }} ({{ $item->result_text }})
                            @php
                                $loopbr++;
                            @endphp    
                        @endif
                    @endforeach
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>   
    <p>ขอบคุณ</p>
    <p>ARD Audit Alert</p>
</body>
</html>