<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardsController@index');

Auth::routes();
Route::resource('Users', 'UsersController');
Route::resource('Groups', 'GroupsController');
Route::resource('TypeQuestions', 'TypeQuestionsController');
Route::resource('SetAnsQuestions', 'SetAnsQuestionsController');
Route::resource('AnsQuestions', 'AnsQuestionsController');
Route::resource('GroupQuestions', 'GroupQuestionsController');
Route::resource('MainQuestions', 'MainQuestionsController');
Route::resource('DetailQuestions', 'DetailQuestionsController');
Route::resource('PlanAudits', 'PlanAuditsController');
Route::resource('SetInsectDiseases', 'SetInsectDiseasesController');
Route::resource('InsectDiseases', 'InsectDiseasesController');

Route::get('/InsectDiseases/createdetail/{insect_disease_id}', 'InsectDiseasesController@createdetail');
Route::Post('/InsectDiseases/createdetailAction/{insect_disease_id}', 'InsectDiseasesController@createdetailAction');
Route::get('/InsectDiseases/editdetail/{insect_disease_case_id}', 'InsectDiseasesController@editdetail');
Route::Post('/InsectDiseases/editdetailAction/{insect_disease_case_id}', 'InsectDiseasesController@editdetailAction');


Route::get('/PlanAudits/addAuditLand/{plan_audit_id}/{sowing_id}', 'PlanAuditsController@addAuditLand');
Route::get('/PlanAudits/updateAuditForms/{plan_audit_detail_id}', 'PlanAuditsController@updateAuditForms');
Route::Post('/PlanAudits/updateAuditFormsAction/{plan_audit_detail_id}', 'PlanAuditsController@updateAuditFormsAction');
Route::get('/PlanAudits/removedetail/{plan_audit_detail_id}', 'PlanAuditsController@removedetail');

Route::get('/PlanAudits/updateInsectForms/{plan_audit_detail_id}/{audit_result_id}', 'PlanAuditsController@updateInsectForms');
Route::post('/PlanAudits/updateInsectFormsAction/{plan_audit_detail_id}/{audit_result_id}', 'PlanAuditsController@updateInsectFormsAction');

Route::get('/PlanAudits/updategps/{plan_audit_detail_id}/{audit_result_id}/{no}/{lat}/{lng}', 'PlanAuditsController@updategps');
Route::get('/PlanAudits/removegps/{plan_audit_detail_id}/{audit_result_id}/{no}', 'PlanAuditsController@removegps');
Route::get('/PlanAudits/updatestatus/{plan_audit_id}/{status}', 'PlanAuditsController@updatestatus');
Route::get('/PlanAudits/updatedetailstatus/{plan_audit_detail_id}/{status}', 'PlanAuditsController@updatedetailstatus');

Route::get('/PlanAudits/reportPage/{plan_audit_id}', 'PlanAuditsController@reportPage');
Route::get('/Reports/reportPage/{plan_audit_id}', 'ReportController@reportPage');
Route::get('/Reports/reportByFarmerByAudit/{plan_audit_detail_id}', 'ReportController@reportByFarmerByAudit');
Route::get('/Reports/mainAudit/{plan_audit_id}', 'ReportController@mainAudit');
Route::get('/Reports/sendReport/{plan_audit_id}', 'ReportController@sendReport');


Route::get('/Dashboards/index', 'DashboardsController@index');
Route::get('/Dashboards/crop/{crop_id}', 'DashboardsController@crop');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test/index', 'TestController@index');
