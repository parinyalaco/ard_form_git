<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultInsectDiseasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_insect_diseases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('set_insect_disease_id')->nullable();
            $table->integer('insect_disease_id')->nullable();
            $table->integer('plan_audit_detail_id')->nullable();
            $table->string('result_text')->nullable();
            $table->string('result_desc')->nullable();
            $table->string('result_grade')->nullable();
            $table->string('path1')->nullable();
            $table->string('path2')->nullable();
            $table->string('note')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_insect_diseases');
    }
}
