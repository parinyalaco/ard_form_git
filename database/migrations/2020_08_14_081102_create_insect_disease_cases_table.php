<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectDiseaseCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insect_disease_cases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insect_disease_id')->nullable();
            $table->string('name');
            $table->string('desc')->nullable();
            $table->integer('seq')->nullable();
            $table->string('conds')->nullable();
            $table->string('results')->nullable();
            $table->string('status')->nullable();
            $table->string('method')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insect_disease_cases');
    }
}
