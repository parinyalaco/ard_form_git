<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectDiseasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insect_diseases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('set_insect_disease_id')->nullable();
            $table->string('type')->nullable();
            $table->string('name');
            $table->string('desc')->nullable();
            $table->integer('seq')->nullable();
            $table->string('conds')->nullable();
            $table->string('results')->nullable();
            $table->string('result_txts')->nullable();
            $table->string('grades')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insect_diseases');
    }
}
