<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultDetailInsectDiseasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_detail_insect_diseases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('set_insect_disease_id')->nullable();
            $table->integer('insect_disease_id')->nullable();
            $table->integer('insect_disease_case_id')->nullable();
            $table->integer('plan_audit_detail_id')->nullable();
            $table->float('point1')->nullable();
            $table->float('point2')->nullable();
            $table->float('point3')->nullable();
            $table->float('point4')->nullable();
            $table->float('point5')->nullable();
            $table->string('path1')->nullable();
            $table->string('path2')->nullable();
            $table->string('path3')->nullable();
            $table->string('path4')->nullable();
            $table->string('path5')->nullable();
            $table->float('point_result')->nullable();
            $table->string('txt_result')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_detail_insect_diseases');
    }
}
