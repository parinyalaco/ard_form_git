<?php $__env->startSection('title'); ?>
Create new PlanAudit
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<h1><?php echo e($planAuditDetailObj->sowing->userfarmer->broker->code); ?> / <?php echo e($planAuditDetailObj->sowing->userfarmer->farmer->fname); ?> <?php echo e($planAuditDetailObj->sowing->userfarmer->farmer->lname); ?></h1>
<h2><?php echo e($planAuditDetailObj->sowing->inputitem->tradename); ?> ปลูกวันที่ <?php echo e($planAuditDetailObj->sowing->start_date); ?> พื้นที่ <?php echo e($planAuditDetailObj->sowing->current_land); ?> ไร่ อายุ
    <a href="<?php echo e(url('PlanAudits/'.$planAuditDetailObj->plan_audit_id)); ?>" class="btn btn-default pull-right btn-sm">Back</a>
    
   <?php 
         $OldDate = new DateTime($planAuditDetailObj->sowing->start_date);
    $now = new DateTime(Date('Y-m-d'));
    $result = $OldDate->diff($now);
    echo $result->days;
     ?>
    วัน</h2>
    <hr/>

    <?php echo Form::open(['url' => 'PlanAudits/updateAuditFormsAction/'.$plan_audit_detail_id, 'class' => 'form-horizontal', 'files' => true]); ?>


    
    <div class="table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th width="50%">Name</th> 
                    <th>Type</th> 
                    <th width="15%">Answer</th>
                    <th>Note</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $maintext = "";
                 ?>
                <?php $__currentLoopData = $planAuditDetailObj->auditresult()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($maintext <> $item->detailquestion->mainquestion->name ): ?>
                        <?php 
                            $maintext = $item->detailquestion->mainquestion->name
                         ?>
                        <tr>
                        <td colspan="4"><b><?php echo e($maintext); ?></b></td>
                        </tr>                        
                    <?php endif; ?>
                    <tr>
                    <td>
                    <?php if(trim($item->detailquestion->link) == 'Y'): ?>

                    <a href="<?php echo e(url('/PlanAudits/updateInsectForms/'.$plan_audit_detail_id."/".$item->detailquestion->id)); ?>"><?php echo e($item->detailquestion->name); ?></a>
                    <?php else: ?>
                        <?php echo e($item->detailquestion->name); ?>

                    <?php endif; ?>                    
                    </td>
                    <td><?php echo e($item->detailquestion->typequestion->name); ?></td>    
                    <td>
                        <?php if(isset($item->result_id)): ?>
            <?php echo Form::select('answer_id_'.$item->id, $answerset[$item->detailquestion->set_ans_question_id],$item->result_id, ['class' => 'form-control caldate getorderlist getprice','placeholder' => '=เลือก=']); ?>   
        <?php else: ?>
            <?php echo Form::select('answer_id_'.$item->id, $answerset[$item->detailquestion->set_ans_question_id],null, ['class' => 'form-control caldate getorderlist  getprice','placeholder' => '=เลือก=']); ?>

        <?php endif; ?>
                       

                    </td>
                    <td>
                        <?php echo Form::file('path1_'.$item->id, $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']);; ?>

                        <?php if(!empty($item->path1)): ?>
                    <a href="<?php echo e(url($item->path1)); ?>" target="_blank"><img height="20px" src="<?php echo e(url($item->path1)); ?>" ></a>
                     <?php echo Form::checkbox('remove_'.$item->id,'remove',false);; ?> remove
                        <?php endif; ?>
                    <input class="form-control" name="note_<?php echo e($item->id); ?>" type="text" id="note_<?php echo e($item->id); ?>" value="<?php echo e(isset($item->note) ? $item->note : ''); ?>" >
                    </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td colspan="4">
                        <?php echo Form::file('path1', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']);; ?>

                        <?php if(!empty($planAuditDetailObj->path1)): ?>
                            <a href="<?php echo e(url($planAuditDetailObj->path1)); ?>" target="_blank"><img height="20px" src="<?php echo e(url($planAuditDetailObj->path1)); ?>" ></a>
                            <?php echo Form::checkbox('removepath1','remove',false);; ?> remove
                        <?php endif; ?>
                        <?php echo Form::file('path2', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']);; ?>

                        <?php if(!empty($planAuditDetailObj->path2)): ?>
                            <a href="<?php echo e(url($planAuditDetailObj->path2)); ?>" target="_blank"><img height="20px" src="<?php echo e(url($planAuditDetailObj->path2)); ?>" ></a>
                            <?php echo Form::checkbox('removepath2','remove',false);; ?> remove
                        <?php endif; ?>
                        <?php echo Form::file('path3', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']);; ?>

                        <?php if(!empty($planAuditDetailObj->path3)): ?>
                            <a href="<?php echo e(url($planAuditDetailObj->path3)); ?>" target="_blank"><img height="20px" src="<?php echo e(url($planAuditDetailObj->path3)); ?>" ></a>
                            <?php echo Form::checkbox('removepath3','remove',false);; ?> remove
                        <?php endif; ?>
                        <?php echo Form::file('path4', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']);; ?>

                        <?php if(!empty($planAuditDetailObj->path4)): ?>
                            <a href="<?php echo e(url($planAuditDetailObj->path4)); ?>" target="_blank"><img height="20px" src="<?php echo e(url($planAuditDetailObj->path4)); ?>" ></a>
                            <?php echo Form::checkbox('removepath4','remove',false);; ?> remove
                        <?php endif; ?>
                        <input class="form-control" name="note" type="text" id="note" value="<?php echo e(isset($planAuditDetailObj->note) ? $planAuditDetailObj->note : ''); ?>" >
                    
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            <?php echo Form::submit('Create', ['class' => 'btn btn-primary form-control']); ?>

        </div>
    </div>
    <?php echo Form::close(); ?>


    <?php if($errors->any()): ?>
        <ul class="alert alert-danger">
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>