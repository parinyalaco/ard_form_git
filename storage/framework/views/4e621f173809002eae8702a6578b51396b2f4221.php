<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    tr th {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    tr th.noborder {
        border: none;
        word-wrap: normal;
    }
     tr th.noborder-last {
        border: none;
        word-wrap: normal;
    }
    tr th.noborderr {
        border: none;
        text-align: right;
        word-wrap: break-word;
    }
    tr th.noborderc {
        border: none;
        text-align: center;
        word-wrap: break-word;
        font: bolder;
    }
    tr td {
        border: 1px solid #000000;
        word-wrap: normal;
    }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>ประเด็นที่ไม่สอดคล้อง</th>
                <th>ระดับ CAR</th>
                <th>รายชื่อเกษตรกรที่พบ</th>
                <th>รายละเอียด</th>
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $mainquestions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mainquestion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td>
                    <?php echo e($mainquestion->name); ?>

                </td>
                <td>
                    <?php echo e($mainquestion->typequestion->name); ?>

                </td>
                <td></td>
                <td></td>
            </tr>
            <?php $__currentLoopData = $data[$mainquestion->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subitem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td></td>
                <td></td>
            <td>
                <?php echo e($subitem->planauditdetail->sowing->userfarmer->farmer->fname); ?> <?php echo e($subitem->planauditdetail->sowing->userfarmer->farmer->lname); ?>

                ปลูกวันที่ <?php echo e($subitem->planauditdetail->sowing->start_date); ?> ขนาด <?php echo e($subitem->planauditdetail->sowing->current_land); ?> ไร่ 
            </td>
                <td><?php echo e($subitem->note); ?></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</body>
</html>