<div class="col-md-4  <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="name" class="control-label"><?php echo e('name'); ?></label>
    <input class="form-control" name="name" type="text" id="name" required value="<?php echo e(isset($GroupQuestion->name) ? $GroupQuestion->name : ''); ?>" >
    <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

</div>
<?php 
    $statuslist = array(
        'Active' => 'Active',
        'InActive' => 'InActive'
    );
 ?>
<div class="col-md-4 <?php echo e($errors->has('status') ? 'has-error' : ''); ?>">
        <?php echo Form::label('status', 'status', ['class' => 'control-label']); ?>

        <?php if(isset($AnsQuestion->set_ans_question_id)): ?>
            <?php echo Form::select('status', $statuslist,$GroupQuestion->status, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('status', $statuslist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('status', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('desc') ? 'has-error' : ''); ?>">
    <label for="desc" class="control-label"><?php echo e('รายละเอียด'); ?></label>
    <textarea class="form-control" name="desc" type="text" id="desc"><?php echo e(isset($GroupQuestion->desc) ? $GroupQuestion->desc : ''); ?></textarea>
    <?php echo $errors->first('desc', '<p class="help-block">:message</p>'); ?>

</div>