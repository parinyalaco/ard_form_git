<?php $__env->startSection('title'); ?>
PlanAudit
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <h1>PlanAudits <a href="<?php echo e(url('PlanAudits/create')); ?>" class="btn btn-primary pull-right btn-sm">Add New PlanAudit</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblPlanAudits">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Num. of Audit</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $PlanAudits; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($item->id); ?></td>
                    <th><?php echo e($item->plan_date); ?></th>
                    <th><?php echo e($item->name); ?></th>
                    <th><?php echo e($item->planauditdetail()->count()); ?></th>
                    <th><?php echo e($item->status); ?></th>
                    <td>
                        <a href="<?php echo e(url('PlanAudits/' . $item->id )); ?>" class="btn btn-primary btn-xs">Manage</a> 
                        
                        <a href="<?php echo e(url('PlanAudits/' . $item->id . '/edit')); ?>" class="btn btn-primary btn-xs">Update</a> 
                        <?php echo Form::open([
                            'method'=>'DELETE',
                            'url' => ['PlanAudits', $item->id],
                            'style' => 'display:inline'
                        ]); ?>

                            <?php echo Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']); ?>

                        <?php echo Form::close(); ?>

                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblPlanAudits').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>