<?php 
    $statuslist = array(
    'Active' => 'Active',
    'Inactive' => 'Inactive',
);
 ?>
<div class="col-md-4 <?php echo e($errors->has('group_question_id') ? 'has-error' : ''); ?>">
        <?php echo Form::label('group_question_id', 'Group Question', ['class' => 'control-label']); ?>

        <?php if(isset($PlanAudit->main_question_id)): ?>
            <?php echo Form::select('group_question_id', $groupquestionlist,$PlanAudit->group_question_id, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('group_question_id', $groupquestionlist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('group_question_id', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4 <?php echo e($errors->has('set_insect_disease_id') ? 'has-error' : ''); ?>">
        <?php echo Form::label('set_insect_disease_id', 'Insect Disease', ['class' => 'control-label']); ?>

        <?php if(isset($PlanAudit->set_insect_disease_id)): ?>
            <?php echo Form::select('set_insect_disease_id', $setinsectdiseaselist,$PlanAudit->set_insect_disease_id, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('set_insect_disease_id', $setinsectdiseaselist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('set_insect_disease_id', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4  <?php echo e($errors->has('plan_date') ? 'has-error' : ''); ?>">
    <label for="plan_date" class="control-label"><?php echo e('Plan Date'); ?></label>
    <input class="form-control"  name="plan_date" type="date" id="plan_date" required value="<?php echo e(isset($PlanAudit->plan_date) ? $PlanAudit->plan_date : ''); ?>" >
    <?php echo $errors->first('plan_date', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4  <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="name" class="control-label"><?php echo e('name'); ?></label>
    <input class="form-control" name="name" type="text" id="name" required value="<?php echo e(isset($PlanAudit->name) ? $PlanAudit->name : ''); ?>" >
    <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4 <?php echo e($errors->has('status') ? 'has-error' : ''); ?>">
        <?php echo Form::label('status', 'Group Question', ['class' => 'control-label']); ?>

        <?php if(isset($PlanAudit->status)): ?>
            <?php echo Form::select('status', $statuslist,$PlanAudit->status, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('status', $statuslist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('status', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-12  <?php echo e($errors->has('desc') ? 'has-error' : ''); ?>">
    <label for="desc" class="control-label"><?php echo e('รายละเอียด'); ?></label>
    <textarea class="form-control" name="desc" type="text" id="desc"><?php echo e(isset($PlanAudit->desc) ? $PlanAudit->desc : ''); ?></textarea>
    <?php echo $errors->first('desc', '<p class="help-block">:message</p>'); ?>

</div>