<div class="col-md-4 <?php echo e($errors->has('set_insect_disease_id') ? 'has-error' : ''); ?>">
        <?php echo Form::label('set_insect_disease_id', 'Set', ['class' => 'control-label']); ?>

        <?php if(isset($InsectDisease->set_insect_disease_id)): ?>
            <?php echo Form::select('set_insect_disease_id', $setInsectDiseaseList,$InsectDisease->set_insect_disease_id, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('set_insect_disease_id', $setInsectDiseaseList,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('set_insect_disease_id', '<p class="help-block">:message</p>'); ?>

</div>
<?php 
    $typeList = array(
        'แมลง' => 'แมลง',
        'โรค' => 'โรค',
    );
 ?>
<div class="col-md-4 <?php echo e($errors->has('type') ? 'has-error' : ''); ?>">
        <?php echo Form::label('type', 'Type', ['class' => 'control-label']); ?>

        <?php if(isset($InsectDisease->type)): ?>
            <?php echo Form::select('type', $typeList,$InsectDisease->type, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('type', $typeList,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('type', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4  <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="name" class="control-label"><?php echo e('name'); ?></label>
    <input class="form-control" name="name" type="text" id="name" required value="<?php echo e(isset($InsectDisease->name) ? $InsectDisease->name : ''); ?>" >
    <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('desc') ? 'has-error' : ''); ?>">
    <label for="desc" class="control-label"><?php echo e('รายละเอียด'); ?></label>
    <textarea class="form-control" name="desc" type="text" id="desc"><?php echo e(isset($InsectDisease->desc) ? $InsectDisease->desc : ''); ?></textarea>
    <?php echo $errors->first('desc', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('conds') ? 'has-error' : ''); ?>">
    <label for="conds" class="control-label"><?php echo e('เงื่อนไข'); ?></label>
    <textarea class="form-control" name="conds" type="text" id="conds"><?php echo e(isset($InsectDisease->conds) ? $InsectDisease->conds : ''); ?></textarea>
    <?php echo $errors->first('conds', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('results') ? 'has-error' : ''); ?>">
    <label for="results" class="control-label"><?php echo e('รายการระดับความเสียหายเนื่องจากศัตรูพืช'); ?></label>
    <textarea class="form-control" name="results" type="text" id="results"><?php echo e(isset($InsectDisease->results) ? $InsectDisease->results : ''); ?></textarea>
    <?php echo $errors->first('results', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('result_txts') ? 'has-error' : ''); ?>">
    <label for="result_txts" class="control-label"><?php echo e('รายการเกณฑ์ประเมิน'); ?></label>
    <textarea class="form-control" name="result_txts" type="text" id="result_txts"><?php echo e(isset($InsectDisease->result_txts) ? $InsectDisease->result_txts : ''); ?></textarea>
    <?php echo $errors->first('result_txts', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('grades') ? 'has-error' : ''); ?>">
    <label for="grades" class="control-label"><?php echo e('รายการเกรด'); ?></label>
    <textarea class="form-control" name="grades" type="text" id="grades"><?php echo e(isset($InsectDisease->grades) ? $InsectDisease->grades : ''); ?></textarea>
    <?php echo $errors->first('grades', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-4  <?php echo e($errors->has('seq') ? 'has-error' : ''); ?>">
    <label for="seq" class="control-label"><?php echo e('Seq'); ?></label>
    <input class="form-control" name="seq" type="number" id="seq" required value="<?php echo e(isset($InsectDisease->seq) ? $InsectDisease->seq : ''); ?>" >
    <?php echo $errors->first('seq', '<p class="help-block">:message</p>'); ?>

</div>

<?php 
    $statuslist = array(
        'Active' => 'Active',
        'InActive' => 'InActive'
    );
 ?>
<div class="col-md-4 <?php echo e($errors->has('status') ? 'has-error' : ''); ?>">
        <?php echo Form::label('status', 'status', ['class' => 'control-label']); ?>

        <?php if(isset($InsectDisease->status)): ?>
            <?php echo Form::select('status', $statuslist,$InsectDisease->status, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('status', $statuslist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('status', '<p class="help-block">:message</p>'); ?>

</div>