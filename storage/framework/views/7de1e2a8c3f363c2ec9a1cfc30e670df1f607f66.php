<?php $__env->startSection('title'); ?>
Create new InsectDisease
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


    <h1>Create New InsectDisease</h1>
        <h2>รายการ <?php echo e($InsectDisease->name); ?></h2>
<h3>Set:<?php echo e($InsectDisease->setinsectdisease->name); ?> / Type:<?php echo e($InsectDisease->type); ?> / Seq:<?php echo e($InsectDisease->seq); ?> / Status:<?php echo e($InsectDisease->status); ?></h3>
    <a href="<?php echo e(url('InsectDiseases/'.$InsectDisease->id)); ?>" class="btn btn-default pull-right btn-sm">Back</a>
    <hr/>

    <?php echo Form::open(['url' => 'InsectDiseases/createdetailAction/'.$InsectDisease->id, 'class' => 'form-horizontal']); ?>


    <?php echo $__env->make('backend.InsectDiseases.formsdetail', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            <?php echo Form::submit('Create', ['class' => 'btn btn-primary form-control']); ?>

        </div>
    </div>
    <?php echo Form::close(); ?>


    <?php if($errors->any()): ?>
        <ul class="alert alert-danger">
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>