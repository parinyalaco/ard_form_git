<?php $__env->startSection('title'); ?>
Create new PlanAudit
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="container">



    <div class="row">

<h1><?php echo e($planAuditDetailObj->sowing->userfarmer->broker->code); ?> / <?php echo e($planAuditDetailObj->sowing->userfarmer->farmer->fname); ?> <?php echo e($planAuditDetailObj->sowing->userfarmer->farmer->lname); ?></h1>
<h2><?php echo e($planAuditDetailObj->sowing->inputitem->tradename); ?> ปลูกวันที่ <?php echo e($planAuditDetailObj->sowing->start_date); ?> พื้นที่ <?php echo e($planAuditDetailObj->sowing->current_land); ?> ไร่ อายุ
    <?php 
         $OldDate = new DateTime($planAuditDetailObj->sowing->start_date);
    $now = new DateTime(Date('Y-m-d'));
    $result = $OldDate->diff($now);
    echo $result->days;
     ?>
    วัน <a href="<?php echo e(url('PlanAudits/updateAuditForms/'.$planAuditDetailObj->id)); ?>" class="btn btn-default pull-right btn-sm">Back</a></h2>
     
    <hr/>

    <?php echo Form::open(['url' => 'PlanAudits/updateInsectFormsAction/'.$plan_audit_detail_id.'/'.$audit_result_id, 'class' => 'form-horizontal', 'files' => true]); ?>


    
    <div class="table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th> 
                    <th>Cases</th>
                    <th>Point 1</th>
                    <th>Point 2</th>
                    <th>Point 3</th>
                    <th>Point 4</th>
                    <th>Point 5</th>
                    <th>รวมคะแนน</th>
                    <th>ระดับความเสียหายเนื่องจากศัตรูพืช</th>
                    <th>ระดับความเสียหาย</th>
                    <th>เกณฑ์ประเมิน</th>
                    <th>เกรดเกษตรกร</th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $planAuditDetailObj->resultinsectdisease; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mainitem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td colspan="9">
                        <h4>No. <?php echo e($mainitem->insectdisease->seq); ?> ประเภท: <?php echo e($mainitem->insectdisease->type); ?> ชื่อ: <?php echo e($mainitem->insectdisease->name); ?></h4>
                            <?php echo Form::file('path1_'.$mainitem->id, $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']);; ?> 
                            <?php if(!empty($mainitem->path1)): ?>
                                <a href="<?php echo e(url($mainitem->path1)); ?>" target="_blank"><img height="20px" src="<?php echo e(url($mainitem->path1)); ?>" ></a>
                                <?php echo Form::checkbox('remove_path1_'.$mainitem->id,'remove',false);; ?> remove
                            <?php endif; ?>
                            <?php echo Form::file('path2_'.$mainitem->id, $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']);; ?>

                            <?php if(!empty($mainitem->path2)): ?>
                                <a href="<?php echo e(url($mainitem->path2)); ?>" target="_blank"><img height="20px" src="<?php echo e(url($mainitem->path2)); ?>" ></a>
                                <?php echo Form::checkbox('remove_path2_'.$mainitem->id,'remove',false);; ?> remove
                            <?php endif; ?>
                        </td>
                        
                    <td style="vertical-align: middle;" rowspan="<?php echo e($mainitem->resultdetailinsectdisease()->count()+1); ?>" >
                        <input class="form-control" name="allresult_txt_<?php echo e($mainitem->id); ?>" type="text" id="allresult_txt_<?php echo e($mainitem->id); ?>" readonly value="<?php echo e($mainitem->result_text); ?>" >
                    </td>
                    <td style="vertical-align: middle;"  rowspan="<?php echo e($mainitem->resultdetailinsectdisease()->count()+1); ?>">
                        <input class="form-control" name="allresult_case_<?php echo e($mainitem->id); ?>" type="text" id="allresult_case_<?php echo e($mainitem->id); ?>" readonly value="<?php echo e($mainitem->result_desc); ?>" >
                    </td>
                    <td style="vertical-align: middle;" rowspan="<?php echo e($mainitem->resultdetailinsectdisease()->count()+1); ?>">
                        <input class="form-control" name="allresult_grade_<?php echo e($mainitem->id); ?>" type="text" id="allresult_grade_<?php echo e($mainitem->id); ?>" readonly value="<?php echo e($mainitem->result_grade); ?>" >
                    </td>
                        
                        
                </tr>
                
                <?php $__currentLoopData = $mainitem->resultdetailinsectdisease; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    
                
                    <tr>
                        <td><?php echo e($item->insectdiseasecase->seq); ?></td>
                        <td><?php echo e($item->insectdiseasecase->name); ?></td>
                    <td><input class="form-control formular<?php echo e($item->id); ?>"  name="point1_<?php echo e($item->id); ?>" type="number" id="point1_<?php echo e($item->id); ?>"  value="<?php echo e($item->point1); ?>" ></td>
                        <td><input class="form-control formular<?php echo e($item->id); ?>"  name="point2_<?php echo e($item->id); ?>" type="number" id="point2_<?php echo e($item->id); ?>"  value="<?php echo e($item->point2); ?>" ></td>
                        <td><input class="form-control formular<?php echo e($item->id); ?>"  name="point3_<?php echo e($item->id); ?>" type="number" id="point3_<?php echo e($item->id); ?>"  value="<?php echo e($item->point3); ?>" ></td>
                        <td>
                            <input class="form-control formular<?php echo e($item->id); ?>"  name="point4_<?php echo e($item->id); ?>" type="number" id="point4_<?php echo e($item->id); ?>" 
                            <?php if($planAuditDetailObj->sowing->current_land < 3): ?>
                                readonly
                            <?php endif; ?> 
                            value="<?php echo e($item->point4); ?>" >
                        </td>
                        <td><input class="form-control formular<?php echo e($item->id); ?>"  name="point5_<?php echo e($item->id); ?>" type="number" id="point5_<?php echo e($item->id); ?>" 
                            <?php if($planAuditDetailObj->sowing->current_land < 3): ?>
                                readonly
                            <?php endif; ?> 
                             value="<?php echo e($item->point5); ?>" >
                        </td>
                        <td><input class="form-control result<?php echo e($item->id); ?>"  name="result_<?php echo e($item->id); ?>" type="text" id="result_<?php echo e($item->id); ?>" readonly value="<?php echo e($item->point_result); ?>" ></td>
                        <td><input class="form-control maintxt<?php echo e($mainitem->id); ?> result_txt<?php echo e($item->id); ?>"  name="result_txt_<?php echo e($item->id); ?>" type="text" id="result_txt_<?php echo e($item->id); ?>" readonly value="<?php echo e($item->txt_result); ?>" ></td>
                     
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td colspan="12"><input class="form-control" name="note<?php echo e($mainitem->id); ?>" type="text" id="note<?php echo e($mainitem->id); ?>" value="<?php echo e($mainitem->note); ?>" >
                    </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>

    <div class="form-group">
        <div class="col-sm-3">
            <?php echo Form::submit('บันทึก', ['class' => 'btn btn-primary form-control']); ?>

        </div>
    </div>
    <?php echo Form::close(); ?>


    <?php if($errors->any()): ?>
        <ul class="alert alert-danger">
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    <?php endif; ?>
    </div>
</div>
    <script>
        $(document).ready(function() {
            <?php $__currentLoopData = $planAuditDetailObj->resultinsectdisease; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mainitem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $__currentLoopData = $mainitem->resultdetailinsectdisease; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    $( ".formular<?php echo e($item->id); ?>" ).change(function() {
                        //alert( "Handler for .change() called." );
                        var sum = 0;
                        var result = 0;
                        $('.formular<?php echo e($item->id); ?>').each(function(){
                            if($(this).val() != ''){
                                sum += parseFloat($(this).val());  // Or this.innerHTML, this.innerText
                            }
                            
                        });
                        <?php if($planAuditDetailObj->sowing->current_land >= 3): ?> 
                            <?php if($item->insectdiseasecase->method == 'Average'): ?>
                                result = sum/5;
                            <?php else: ?>
                                result = sum;
                            <?php endif; ?> 
                        <?php else: ?> 
                            <?php if($item->insectdiseasecase->method == 'Average'): ?>
                                result = sum/3;
                            <?php else: ?>
                                result = sum;
                            <?php endif; ?> 
                        <?php endif; ?>
                        $('#result_<?php echo e($item->id); ?>').val(result);
                        <?php 
                            $func = explode(",",$item->insectdiseasecase->conds);
                           // var_dump($func);
                           echo "if(".$func[0]."){ result_txt='".$func[1]."'}else{if(".$func[2]."){ result_txt='".$func[3]."'}else{ result_txt='".$func[4]."'}}";
                         ?>
                        $('#result_txt_<?php echo e($item->id); ?>').val(result_txt);
                        _recal<?php echo e($mainitem->id); ?>();
                    });
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                function _recal<?php echo e($mainitem->id); ?>(){
                    var mainratetxt = 'GE';
                    var maintxtcase = '';
                    var maintxtgrade = '';
                    $('.maintxt<?php echo e($mainitem->id); ?>').each(function(){
                        if($(this).val() == ''){

                        }else{
                            if($(this).val() == 'EL'){
                                mainratetxt = 'EL';
                            }else{
                                if($(this).val() == 'ET'){
                                    if(mainratetxt == 'ET' || mainratetxt == 'GE'){
                                        mainratetxt = 'ET';
                                    }
                                }else{
                                    if(mainratetxt == 'GE'){
                                        mainratetxt = 'GE';
                                    }
                                }    
                            }
                        }
                    });
                    switch (mainratetxt){
                    <?php 
                        $casestep2 = explode(",",$mainitem->insectdisease->result_txts);
                       // var_dump($casestep2);
                        foreach($casestep2 as $key=>$caseobj){
                            $subcase = explode(":",$caseobj);
                        //    var_dump($subcase);
                            echo "case '".$subcase[0]."': maintxtcase = '".$subcase[1]."'; break; ";
                        }
                     ?>
                    }
                    $('#allresult_txt_<?php echo e($mainitem->id); ?>').val(mainratetxt);
                    $('#allresult_case_<?php echo e($mainitem->id); ?>').val(maintxtcase);
                    switch (maintxtcase){
                    <?php 
                        $casestep2 = explode(",",$mainitem->insectdisease->grades);
                       // var_dump($casestep2);
                        foreach($casestep2 as $key=>$caseobj){
                            $subcase = explode(":",$caseobj);
                        //    var_dump($subcase);
                            echo "case '".$subcase[0]."': maintxtgrade = '".$subcase[1]."'; break; ";
                        }
                     ?>
                    }
                    $('#allresult_grade_<?php echo e($mainitem->id); ?>').val(maintxtgrade);
                }

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>