<div class="col-md-4  <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="name" class="control-label"><?php echo e('name'); ?></label>
    <input class="form-control" name="name" type="text" id="name" required value="<?php echo e(isset($TypeQuestion->name) ? $TypeQuestion->name : ''); ?>" >
    <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4  <?php echo e($errors->has('role') ? 'has-error' : ''); ?>">
    <label for="role" class="control-label"><?php echo e('Role'); ?></label>
    <input class="form-control" name="role" type="text" id="role" required value="<?php echo e(isset($TypeQuestion->role) ? $TypeQuestion->role : ''); ?>" >
    <?php echo $errors->first('role', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('desc') ? 'has-error' : ''); ?>">
    <label for="desc" class="control-label"><?php echo e('รายละเอียดรถ'); ?></label>
    <textarea class="form-control" name="desc" type="text" id="desc"><?php echo e(isset($TypeQuestion->desc) ? $TypeQuestion->desc : ''); ?></textarea>
    <?php echo $errors->first('desc', '<p class="help-block">:message</p>'); ?>

</div>