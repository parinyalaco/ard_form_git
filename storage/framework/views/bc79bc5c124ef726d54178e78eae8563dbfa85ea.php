
<div class="col-md-4 <?php echo e($errors->has('group_question_id') ? 'has-error' : ''); ?>">
        <?php echo Form::label('group_question_id', 'status', ['class' => 'control-label']); ?>

        <?php if(isset($MainQuestion->group_question_id)): ?>
            <?php echo Form::select('group_question_id', $qroupquestionlist,$MainQuestion->group_question_id, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('group_question_id', $qroupquestionlist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('group_question_id', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-1  <?php echo e($errors->has('seq') ? 'has-error' : ''); ?>">
    <label for="seq" class="control-label"><?php echo e('seq'); ?></label>
    <input class="form-control"  name="seq" type="number" id="seq" required value="<?php echo e(isset($MainQuestion->seq) ? $MainQuestion->seq : ''); ?>" >
    <?php echo $errors->first('seq', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-7  <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="name" class="control-label"><?php echo e('name'); ?></label>
    <input class="form-control" name="name" type="text" id="name" required value="<?php echo e(isset($MainQuestion->name) ? $MainQuestion->name : ''); ?>" >
    <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('desc') ? 'has-error' : ''); ?>">
    <label for="desc" class="control-label"><?php echo e('รายละเอียด'); ?></label>
    <textarea class="form-control" name="desc" type="text" id="desc"><?php echo e(isset($MainQuestion->desc) ? $MainQuestion->desc : ''); ?></textarea>
    <?php echo $errors->first('desc', '<p class="help-block">:message</p>'); ?>

</div>