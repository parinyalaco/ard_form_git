<?php $__env->startSection('title'); ?>
InsectDisease
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <h1>รายการ <?php echo e($InsectDisease->name); ?></h1>
<h2>Set:<?php echo e($InsectDisease->setinsectdisease->name); ?> / Type:<?php echo e($InsectDisease->type); ?> / Seq:<?php echo e($InsectDisease->seq); ?> / Status:<?php echo e($InsectDisease->status); ?></h2>
    
    <div class="table-responsive">
        
        <a href="<?php echo e(url('InsectDiseases')); ?>" class="btn btn-default pull-right btn-sm">Back</a>
        <a href="<?php echo e(url('InsectDiseases/createdetail/'.$InsectDisease->id)); ?>" class="btn btn-primary pull-right btn-sm">Add New InsectDisease</a>
    
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>seq.</th>
                    <th>Name</th>
                    <th>Method</th>
                    <th>desc</th>
                    <th>Status</th> 
                    <th>Action</th> 
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $InsectDisease->insectdiseasecase; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                    <td><?php echo e($item->seq); ?></td> 
                    <td><?php echo e($item->name); ?></td> 
                    <td><?php echo e($item->method); ?></td> 
                    <td><?php echo nl2br(e($item->desc)); ?></td>
                    <td><?php echo e($item->status); ?></td> 
                    <td>
                        <a href="<?php echo e(url('InsectDiseases/editdetail/' . $item->id )); ?>" class="btn btn-primary btn-xs">Update</a> 
                        <?php echo Form::open([
                            'method'=>'DELETE',
                            'url' => ['InsectDiseaseCases', $item->id],
                            'style' => 'display:inline'
                        ]); ?>

                            <?php echo Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']); ?>

                        <?php echo Form::close(); ?>

                    </td> 
                </tr>  
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>    
        </table>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>