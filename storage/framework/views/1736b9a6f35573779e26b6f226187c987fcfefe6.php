<?php $__env->startSection('title'); ?>
DetailQuestion
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <h1>DetailQuestions <a href="<?php echo e(url('DetailQuestions/create')); ?>" class="btn btn-primary pull-right btn-sm">Add New DetailQuestion</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblDetailQuestions">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Main<br/>Name</th>
                    <th>Type</th>
                    <th>Set</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $DetailQuestions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($item->id); ?></td>
                    <td><?php echo e($item->mainquestion->name); ?><br/><?php echo e($item->name); ?></td>
                    <td><?php echo e($item->typequestion->name); ?></td>
                    <td><?php echo e($item->setansquestion->name); ?></td>
                    
                    <td>
                        <a href="<?php echo e(url('DetailQuestions/' . $item->id . '/edit')); ?>" class="btn btn-primary btn-xs">Update</a> 
                        <?php echo Form::open([
                            'method'=>'DELETE',
                            'url' => ['DetailQuestions', $item->id],
                            'style' => 'display:inline'
                        ]); ?>

                            <?php echo Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']); ?>

                        <?php echo Form::close(); ?>

                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblDetailQuestions').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>