<?php $__env->startSection('title'); ?>
Edit InsectDisease
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <h1>Edit InsectDisease</h1>
    <a href="<?php echo e(url('InsectDiseases/'.$InsectDiseaseCase->insect_disease_id)); ?>" class="btn btn-default pull-right btn-sm">Back</a>
    <hr/>

    <?php echo Form::open(['url' => 'InsectDiseases/editdetailAction/'.$InsectDiseaseCase->id, 'class' => 'form-horizontal']); ?>



    <?php echo $__env->make('backend.InsectDiseases.formsdetail', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            <?php echo Form::submit('Update', ['class' => 'btn btn-primary form-control']); ?>

        </div>
    </div>
    <?php echo Form::close(); ?>


    <?php if($errors->any()): ?>
        <ul class="alert alert-danger">
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>