<?php $__env->startSection('title'); ?>
GroupQuestion
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <h1>GroupQuestion</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> 
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo e($GroupQuestion->id); ?></td> 
                </tr>
            </tbody>    
        </table>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>