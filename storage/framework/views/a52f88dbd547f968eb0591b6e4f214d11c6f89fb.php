<?php $__env->startSection('title'); ?>
PlanAudit
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<h1>หน้าจัดการ Audit <?php echo e($PlanAudit->name); ?> ของวันที่ <?php echo e($PlanAudit->plan_date); ?> (<?php echo e($PlanAudit->status); ?>)</h1>
<h3><?php echo e($PlanAudit->desc); ?> <a href="<?php echo e(url('PlanAudits/reportPage/'.$PlanAudit->id)); ?>" class="btn btn-default pull-right btn-sm">Report</a></h3>

<h1>รายการลูกสวนที่เลือกมาตรวจสอบ</h1>
<div class="table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr> <th>Crop</th> 
                    <th>Broker</th> 
                    <th>Farmer</th> 
                    <th>พันธุ์</th> 
                    <th>วันปลูก</th>
                    <th>อายุ</th>
                    <th>สภาพ</th>
                    <th>พื้นที่</th> 
                    <th>ผล</th> 
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $PlanAudit->planauditdetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                      <td><?php echo e($item->sowing->crop->name); ?></td> 
                    <td><?php echo e($item->sowing->userfarmer->broker->code); ?></td> 
                    <td><?php echo e($item->sowing->userfarmer->farmer->fname); ?> <?php echo e($item->sowing->userfarmer->farmer->lname); ?></td> 
                    
                    <td><?php echo e($item->sowing->InputItem->tradename); ?></td> 
                    <td><?php echo e($item->sowing->start_date); ?></td>  
                    <td>
                        <?php 
                            $OldDate = new DateTime($item->sowing->start_date);
    $now = new DateTime(Date('Y-m-d'));
    $result = $OldDate->diff($now);
    echo $result->days;
                         ?>
                    </td>
                    <td><?php echo e($item->sowing->harvest_status); ?></td>
                    <td><?php echo e($item->sowing->current_land); ?>

                        <?php if(!empty($item->lat) && !empty($item->lng)): ?>
                        <a href="http://www.google.com/maps/place/<?php echo e($item->lat); ?>,<?php echo e($item->lng); ?>" target="_blank">
                            <img src="<?php echo e(url('/img/map.png')); ?>" alt="Image"/></a>
                            
                        <?php endif; ?>
                    </td> 
                    <td>
                        <?php if(!empty($item->result_txt)): ?>
                          <?php echo e($item->result_txt); ?> <br/>
                          เกรด <?php echo e($item->result_grade); ?> / 
                          จากผล <?php echo e($item->result_num); ?>/<?php echo e($item->auditresult()->count()); ?> ข้อ
                        <?php endif; ?>
                        
                    </td>
                <td> <a href="<?php echo e(url('/PlanAudits/updateAuditForms/'.$item->id)); ?>" ><img src="<?php echo e(url('/img/forms.png')); ?>" alt="Image"/> </a>
                            <a href="<?php echo e(url('/PlanAudits/removedetail/'.$item->id)); ?>"><img src="<?php echo e(url('/img/delete.png')); ?>" alt="Image"/></a>
                            </td>
                </tr>  
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
            </tbody>    
        </table>
        <h1>ค้นหาลูกสวน</h1>
        <form method="GET" action="<?php echo e(url('/PlanAudits/'.$PlanAudit->id)); ?>" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
            
                <div class="col-md-4">Farmer : <input type="text" class="form-control" name="search" placeholder="Search..." value="<?php echo e(request('search')); ?>"></div>
                <div class="col-md-4">Crop : 
                    <?php if(request('crop_id') != ''): ?>
                    <?php echo Form::select('crop_id', $croplist,request('crop_id'), ['class' => 'form-control caldate getorderlist getprice']); ?>   
                <?php else: ?>
                    <?php echo Form::select('crop_id', $croplist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

                <?php endif; ?>
            </div>
                <div class="col-md-4">Broker : 
                    <?php if(request('broker_id') != ''): ?>
                    <?php echo Form::select('broker_id', $brokerlist,request('broker_id'), ['class' => 'form-control caldate getorderlist getprice']); ?>   
                <?php else: ?>
                    <?php echo Form::select('broker_id', $brokerlist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

                <?php endif; ?>
            </div>
                <div class="col-md-4">From : 
                <input class="form-control"  name="from_date" type="date" id="from_date" 
                
                <?php if(!empty(request('from_date'))): ?>
                    value="<?php echo e(date('Y-m-d',strtotime(request('from_date')))); ?>"   
                <?php else: ?>
                    value="<?php echo e(date('Y-m-d')); ?>"
                <?php endif; ?>
                >
                </div>
                <div class="col-md-4">To : 
                <input class="form-control"  name="to_date" type="date" id="to_date" 
                <?php if(!empty(request('to_date'))): ?>
                    value="<?php echo e(date('Y-m-d',strtotime(request('to_date')))); ?>"   
                <?php else: ?>
                    value="<?php echo e(date('Y-m-d')); ?>"
                <?php endif; ?>
                >
                </div>
                <div class="col-md-4"><span class="input-group-append">
                    <button class="btn btn-secondary" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </span></div>
            
        </form>
     <div class="table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr> <th>Crop</th> 
                    <th>Broker</th> 
                    <th>Farmer</th> 
                    <th>พันธุ์</th> 
                    <th>วันปลูก</th>
                    <th>อายุ</th>
                    <th>สภาพ</th>
                    <th>พื้นที่</th> 
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $sowingdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                      <td><?php echo e($item->crop->name); ?></td> 
                    <td><?php echo e($item->userfarmer->broker->code); ?></td> 
                    <td><?php echo e($item->userfarmer->farmer->fname); ?> <?php echo e($item->userfarmer->farmer->lname); ?></td> 
                    <td><?php echo e($item->InputItem->tradename); ?></td> 
                    <td><?php echo e($item->start_date); ?></td> 
                    
                    <td>
                        <?php 
                            $OldDate = new DateTime($item->start_date);
    $now = new DateTime(Date('Y-m-d'));
    $result = $OldDate->diff($now);
    echo $result->days;
                         ?>
                    </td>
                    <td><?php echo e($item->harvest_status); ?></td> 
                    <td><?php echo e($item->current_land); ?> 
                        <?php if($item->gpxfiles()->count() > 0): ?>
                            <img src="<?php echo e(url('/img/map.png')); ?>" alt="Image"/>
                        <?php endif; ?>
                        
                    </td> 
                    <td><a href="<?php echo e(url('PlanAudits/addAuditLand/'. $PlanAudit->id .'/' . $item->id  )); ?>" class="btn btn-primary btn-xs">Add</a> 
                        </td>
                </tr>  
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
            </tbody>    
        </table>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backLayout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>