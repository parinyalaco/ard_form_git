
<div class="col-md-4  <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="name" class="control-label"><?php echo e('name'); ?></label>
    <input class="form-control" name="name" type="text" id="name" required value="<?php echo e(isset($InsectDiseaseCase->name) ? $InsectDiseaseCase->name : ''); ?>" >
    <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

</div>
<?php 
    $methodlist = array(
        'Sum' => 'Sum',
        'Average' => 'Avg'
    );
 ?>
<div class="col-md-3 <?php echo e($errors->has('method') ? 'has-error' : ''); ?>">
        <?php echo Form::label('method', 'Method', ['class' => 'control-label']); ?>

        <?php if(isset($InsectDiseaseCase->status)): ?>
            <?php echo Form::select('method', $methodlist,$InsectDiseaseCase->method, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('method', $methodlist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('method', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-2  <?php echo e($errors->has('seq') ? 'has-error' : ''); ?>">
    <label for="seq" class="control-label"><?php echo e('Seq'); ?></label>
    <input class="form-control" name="seq" type="number" id="seq" required value="<?php echo e(isset($InsectDiseaseCase->seq) ? $InsectDiseaseCase->seq : ''); ?>" >
    <?php echo $errors->first('seq', '<p class="help-block">:message</p>'); ?>

</div>

<?php 
    $statuslist = array(
        'Active' => 'Active',
        'InActive' => 'InActive'
    );
 ?>
<div class="col-md-3 <?php echo e($errors->has('status') ? 'has-error' : ''); ?>">
        <?php echo Form::label('status', 'status', ['class' => 'control-label']); ?>

        <?php if(isset($InsectDiseaseCase->status)): ?>
            <?php echo Form::select('status', $statuslist,$InsectDiseaseCase->status, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('status', $statuslist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('status', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-12  <?php echo e($errors->has('desc') ? 'has-error' : ''); ?>">
    <label for="desc" class="control-label"><?php echo e('รายละเอียด'); ?></label>
    <textarea class="form-control" name="desc" type="text" id="desc"><?php echo e(isset($InsectDiseaseCase->desc) ? $InsectDiseaseCase->desc : ''); ?></textarea>
    <?php echo $errors->first('desc', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('conds') ? 'has-error' : ''); ?>">
    <label for="conds" class="control-label"><?php echo e('เงื่อนไข'); ?></label>
    <textarea class="form-control" name="conds" type="text" id="conds"><?php echo e(isset($InsectDiseaseCase->conds) ? $InsectDiseaseCase->conds : ''); ?></textarea>
    <?php echo $errors->first('conds', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('result_text') ? 'has-error' : ''); ?>">
    <label for="result_text" class="control-label"><?php echo e('รายการระดับความเสียหายเนื่องจากศัตรูพืช'); ?></label>
    <textarea class="form-control" name="result_text" type="text" id="result_text"><?php echo e(isset($InsectDiseaseCase->result_text) ? $InsectDiseaseCase->result_text : ''); ?></textarea>
    <?php echo $errors->first('result_text', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('result_desc') ? 'has-error' : ''); ?>">
    <label for="result_desc" class="control-label"><?php echo e('รายการเกณฑ์ประเมิน'); ?></label>
    <textarea class="form-control" name="result_desc" type="text" id="result_desc"><?php echo e(isset($InsectDiseaseCase->result_desc) ? $InsectDiseaseCase->result_desc : ''); ?></textarea>
    <?php echo $errors->first('result_desc', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('result_grade') ? 'has-error' : ''); ?>">
    <label for="result_grade" class="control-label"><?php echo e('รายการเกรด'); ?></label>
    <textarea class="form-control" name="result_grade" type="text" id="result_grade"><?php echo e(isset($InsectDiseaseCase->result_grade) ? $InsectDiseaseCase->result_grade : ''); ?></textarea>
    <?php echo $errors->first('result_grade', '<p class="help-block">:message</p>'); ?>

</div>

