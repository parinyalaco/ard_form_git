
<div class="col-md-4 <?php echo e($errors->has('main_question_id') ? 'has-error' : ''); ?>">
        <?php echo Form::label('main_question_id', 'Main', ['class' => 'control-label']); ?>

        <?php if(isset($DetailQuestion->main_question_id)): ?>
            <?php echo Form::select('main_question_id', $mainquestionlist,$DetailQuestion->main_question_id, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('main_question_id', $mainquestionlist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('main_question_id', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4 <?php echo e($errors->has('type_question_id') ? 'has-error' : ''); ?>">
        <?php echo Form::label('type_question_id', 'Type', ['class' => 'control-label']); ?>

        <?php if(isset($DetailQuestion->type_question_id)): ?>
            <?php echo Form::select('type_question_id', $typequestionlist,$DetailQuestion->type_question_id, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('type_question_id', $typequestionlist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('type_question_id', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-4 <?php echo e($errors->has('set_ans_question_id') ? 'has-error' : ''); ?>">
        <?php echo Form::label('set_ans_question_id', 'Answer Set', ['class' => 'control-label']); ?>

        <?php if(isset($DetailQuestion->set_ans_question_id)): ?>
            <?php echo Form::select('set_ans_question_id', $setansquestionlist,$DetailQuestion->set_ans_question_id, ['class' => 'form-control caldate getorderlist getprice']); ?>   
        <?php else: ?>
            <?php echo Form::select('set_ans_question_id', $setansquestionlist,null, ['class' => 'form-control caldate getorderlist  getprice']); ?>

        <?php endif; ?>
        <?php echo $errors->first('set_ans_question_id', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-1  <?php echo e($errors->has('seq') ? 'has-error' : ''); ?>">
    <label for="seq" class="control-label"><?php echo e('seq'); ?></label>
    <input class="form-control"  name="seq" type="number" id="seq" required value="<?php echo e(isset($DetailQuestion->seq) ? $DetailQuestion->seq : ''); ?>" >
    <?php echo $errors->first('seq', '<p class="help-block">:message</p>'); ?>

</div>
<div class="col-md-11  <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <label for="name" class="control-label"><?php echo e('name'); ?></label>
    <input class="form-control" name="name" type="text" id="name" required value="<?php echo e(isset($DetailQuestion->name) ? $DetailQuestion->name : ''); ?>" >
    <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>

</div>

<div class="col-md-12  <?php echo e($errors->has('desc') ? 'has-error' : ''); ?>">
    <label for="desc" class="control-label"><?php echo e('รายละเอียด'); ?></label>
    <textarea class="form-control" name="desc" type="text" id="desc"><?php echo e(isset($DetailQuestion->desc) ? $DetailQuestion->desc : ''); ?></textarea>
    <?php echo $errors->first('desc', '<p class="help-block">:message</p>'); ?>

</div>